package com.yufea.scalepanel.barcode.builder;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.yufea.scalepanel.config.BarCodeConfig;
import com.yufea.scalepanel.config.TextConfig;
import com.yufea.scalepanel.utils.BarcodeUtil;
import com.yufea.scalepanel.utils.BitmapUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author peifen
 * @date 2020/8/3 18:56
 */
public class EAN13Builder {
    //EAN码对应A、B、C区域
    private static final int[][] A = {{0, 0, 0, 1, 1, 0, 1}, {0, 0, 1, 1, 0, 0, 1}, {0, 0, 1, 0, 0, 1, 1}, {0, 1, 1, 1, 1, 0, 1}, {0, 1, 0, 0, 0, 1, 1},
            {0, 1, 1, 0, 0, 0, 1}, {0, 1, 0, 1, 1, 1, 1}, {0, 1, 1, 1, 0, 1, 1}, {0, 1, 1, 0, 1, 1, 1}, {0, 0, 0, 1, 0, 1, 1}};
    private static final int[][] B = {{0, 1, 0, 0, 1, 1, 1}, {0, 1, 1, 0, 0, 1, 1}, {0, 0, 1, 1, 0, 1, 1}, {0, 1, 0, 0, 0, 0, 1}, {0, 0, 1, 1, 1, 0, 1},
            {0, 1, 1, 1, 0, 0, 1}, {0, 0, 0, 0, 1, 0, 1}, {0, 0, 1, 0, 0, 0, 1}, {0, 0, 0, 1, 0, 0, 1}, {0, 0, 1, 0, 1, 1, 1}};
    private static final int[][] C = {{1, 1, 1, 0, 0, 1, 0}, {1, 1, 0, 0, 1, 1, 0}, {1, 1, 0, 1, 1, 0, 0}, {1, 0, 0, 0, 0, 1, 0}, {1, 0, 1, 1, 1, 0, 0},
            {1, 0, 0, 1, 1, 1, 0}, {1, 0, 1, 0, 0, 0, 0}, {1, 0, 0, 0, 1, 0, 0}, {1, 0, 0, 1, 0, 0, 0}, {1, 1, 1, 0, 1, 0, 0}};

    //前置码
    private static final int[][] front = {{1, 1, 1, 1, 1, 1}, {1, 1, 2, 1, 2, 2}, {1, 1, 2, 2, 1, 2}, {1, 1, 2, 2, 2, 1}, {1, 2, 1, 1, 2, 2},
            {1, 2, 2, 1, 1, 2}, {1, 2, 2, 2, 1, 1}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 2, 1}, {1, 2, 2, 1, 2, 1}};
    private static final String TAG = EAN13Builder.class.getSimpleName();

    /**
     * 获取模块集合，EAN13 有113个模块
     * 0：空白（透明） 1：正常线段   2：分割线
     *
     * @param s
     * @return
     */
    private List<Integer> getModules(String s, int checkCode) {
        List<Integer> modules = new ArrayList<>();
        for (int i = 0; i < 11; i++) { //左右空白区域 11个模块
            modules.add(0);//表示透明
        }
        int[] datas = new int[12];
        for (int i = 0; i < s.length(); i++) {
            datas[i] = s.charAt(i) - 48;
        }
        //起始符 3个模块
        modules.add(2);
        modules.add(0);
        modules.add(2);
        int[] left = front[datas[0]];
        for (int i = 0; i < 6; i++) {
            int[] re;
            if (left[i] == 1) {//A
                re = A[datas[i + 1]];
            } else {//B
                re = B[datas[i + 1]];
            }
            for (Integer in : re) {
                modules.add(in);
            }
        }
        //中间的分隔符 5个模块
        modules.add(0);
        modules.add(2);
        modules.add(0);
        modules.add(2);
        modules.add(0);
        for (int i = 0; i < 5; i++) {
            int[] re = C[datas[i + 7]];
            for (Integer in : re) {
                modules.add(in);
            }
        }
        //校验位
        int[] re = C[checkCode];
        for (Integer in : re) {
            modules.add(in);
        }
        //结尾分隔符
        modules.add(2);
        modules.add(0);
        modules.add(2);
        //结尾空白区域
        for (int i = 0; i < 7; i++) {
            modules.add(0);
        }
        return modules;
    }

    /**
     * 生产条形码
     *
     * @param config BarCodeConfig
     * @return 生产的条形码bitmap
     */
    public Bitmap generateBarcode(BarCodeConfig config) {
        String content = config.text.length() == 13 ? config.text.substring(0, config.text.length() - 1) : config.text;
        int checkCode = BarcodeUtil.getStandardUPCEANChecksum(content);
        List<Integer> modules = getModules(content, checkCode);
        float width = config.width * config.scale;
        float height = config.height * config.scale;

        int unitWidth = (int) (width / modules.size());//将条形码分成113个模块，每一个模块的宽度
        int leftPadding = 0;
        if (unitWidth == 0) {
            unitWidth = 1;
        }
        Bitmap bitmap = Bitmap.createBitmap(unitWidth * modules.size(), (int) height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(config.contentColor);
        for (int i = 0; i < modules.size(); i++) {
            int left = i * unitWidth + leftPadding;
            Integer flag = modules.get(i);
            if (flag == 0) {//空白
            } else if (flag == 1) {//黑色线
                int heightBy1 = (int) (bitmap.getHeight() - (bitmap.getHeight() * 0.23));
                if (!config.isDisplayText) {
                    heightBy1 = (int) (bitmap.getHeight() - (bitmap.getHeight() * 0.11));
                }
                canvas.drawRect(new Rect(left, 0, left + unitWidth, heightBy1), paint);
            } else if (flag == 2) {//分割线
                int heightBy2 = (int) (bitmap.getHeight() - (bitmap.getHeight() * 0.11));
                if (!config.isDisplayText) {
                    heightBy2 = bitmap.getHeight();
                }
                canvas.drawRect(new Rect(left, 0, left + unitWidth, heightBy2), paint);
            }
        }
        if (config.isDisplayText) {
            String s = content + checkCode;
            float left = 0;
            for (int i = 0; i < s.length(); i++) {
                String fontText = s.substring(i, i + 1);

                TextConfig textConfig = new TextConfig();
                textConfig.scale = config.scale;
                textConfig.text = fontText;
                textConfig.width = (42 * unitWidth / 6f) / config.scale;
                textConfig.height = (bitmap.getHeight() * 0.22f) / config.scale;
                textConfig.contentColor = config.contentColor;
                textConfig.isFixedWidth = true;
                Bitmap fontBitmap = BitmapUtil.convertToBitmapByWidthAndHeight(textConfig);
                if (i == 0) {
                    left = 11 * unitWidth - fontBitmap.getWidth();
                } else if (i == 1) {
                    left = 14 * unitWidth;
                } else if (i == 7) {
                    left = 61 * unitWidth;
                } else {
                    left += fontBitmap.getWidth();
                }
                canvas.drawBitmap(fontBitmap, left, bitmap.getHeight() * 0.78f, paint);
            }
        }
        canvas.save();
        return bitmap;
    }
}
