package com.yufea.scalepanel.barcode.builder;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.yufea.scalepanel.config.BarCodeConfig;
import com.yufea.scalepanel.config.TextConfig;
import com.yufea.scalepanel.utils.BarcodeUtil;
import com.yufea.scalepanel.utils.BitmapUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author peifen
 * @date 2020/8/3 18:56
 */
public class EAN8Builder {
    //EAN码对应A、B、C区域
    private static final int[][] A = {{0, 0, 0, 1, 1, 0, 1}, {0, 0, 1, 1, 0, 0, 1}, {0, 0, 1, 0, 0, 1, 1}, {0, 1, 1, 1, 1, 0, 1}, {0, 1, 0, 0, 0, 1, 1},
            {0, 1, 1, 0, 0, 0, 1}, {0, 1, 0, 1, 1, 1, 1}, {0, 1, 1, 1, 0, 1, 1}, {0, 1, 1, 0, 1, 1, 1}, {0, 0, 0, 1, 0, 1, 1}};
    private static final int[][] C = {{1, 1, 1, 0, 0, 1, 0}, {1, 1, 0, 0, 1, 1, 0}, {1, 1, 0, 1, 1, 0, 0}, {1, 0, 0, 0, 0, 1, 0}, {1, 0, 1, 1, 1, 0, 0},
            {1, 0, 0, 1, 1, 1, 0}, {1, 0, 1, 0, 0, 0, 0}, {1, 0, 0, 0, 1, 0, 0}, {1, 0, 0, 1, 0, 0, 0}, {1, 1, 1, 0, 1, 0, 0}};

    /**
     * 获取模块集合，EAN13 有81个模块
     * 0：空白（透明） 1：正常线段   2：分割线
     *
     * @param s
     * @return
     */
    private List<Integer> getModules(String s, int checkCode) {
        List<Integer> modules = new ArrayList<>();
        for (int i = 0; i < 7; i++) { //左空白区域 7个模块
            modules.add(0);//表示透明
        }
        //起始符 3个模块
        modules.add(2);
        modules.add(0);
        modules.add(2);
        int[] datas = new int[7];
        for (int i = 0; i < s.length(); i++) {
            datas[i] = s.charAt(i) - 48;
        }
        for (int i = 0; i < 4; i++) { //左资料码 28个模块
            int[] re = A[datas[i]];
            for (Integer in : re) {
                modules.add(in);
            }
        }
        //中间的分隔符 5个模块
        modules.add(0);
        modules.add(2);
        modules.add(0);
        modules.add(2);
        modules.add(0);
        //有资料码加校验码28个模块
        for (int i = 0; i < 3; i++) {
            int[] re = C[datas[i + 4]];
            for (Integer in : re) {
                modules.add(in);
            }
        }
        //校验位
        int[] re = C[checkCode];
        for (Integer in : re) {
            modules.add(in);
        }
        //结尾分隔符 3个模块
        modules.add(2);
        modules.add(0);
        modules.add(2);
        //结尾空白区域
        for (int i = 0; i < 7; i++) {
            modules.add(0);
        }
        return modules;
    }

    /**
     * 生产条形码
     *
     * @param config BarCodeConfig
     * @return 生产的条形码bitmap
     */
    public Bitmap generateBarcode(BarCodeConfig config) {
        String content = config.text.length() == 8 ? config.text.substring(0, config.text.length() - 1) : config.text;
        int checkCode = BarcodeUtil.getStandardUPCEANChecksum(content);
        List<Integer> modules = getModules(content, checkCode);
        float width = config.width * config.scale;
        float height = config.height * config.scale;

        int unitWidth = (int) (width / modules.size());//将条形码分成113个模块，每一个模块的宽度
        int leftPadding = 0;
        if (unitWidth == 0) {
            unitWidth = 1;
        }
        Bitmap bitmap = Bitmap.createBitmap(unitWidth * modules.size(), (int) height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(config.contentColor);
        for (int i = 0; i < modules.size(); i++) {
            int left = i * unitWidth + leftPadding;
            Integer flag = modules.get(i);
            if (flag == 0) {//空白
            } else if (flag == 1) {//黑色线
                int heightBy1 = (int) (bitmap.getHeight() - (bitmap.getHeight() * 0.23));
                if (!config.isDisplayText) {
                    heightBy1 = (int) (bitmap.getHeight() - (bitmap.getHeight() * 0.11));
                }
                canvas.drawRect(new Rect(left, 0, left + unitWidth, heightBy1), paint);
            } else if (flag == 2) {//分割线
                int heightBy2 = (int) (bitmap.getHeight() - (bitmap.getHeight() * 0.11));
                if (!config.isDisplayText) {
                    heightBy2 = bitmap.getHeight();
                }
                canvas.drawRect(new Rect(left, 0, left + unitWidth, heightBy2), paint);
            }
        }
        if (config.isDisplayText) {
            String s = content + checkCode;
            float left = 0;
            for (int i = 0; i < s.length(); i++) {
                String fontText = s.substring(i, i + 1);

                TextConfig textConfig = new TextConfig();
                textConfig.text = fontText;
                textConfig.scale = config.scale;
                textConfig.width = (28 * unitWidth / 4f) / config.scale;
                textConfig.height = (bitmap.getHeight() * 0.22f) / config.scale;
                textConfig.isFixedWidth = true;
                textConfig.contentColor = config.contentColor;
                Bitmap fontBitmap = BitmapUtil.convertToBitmapByWidthAndHeight(textConfig);
                if (i == 0) {
                    left = 10 * unitWidth;
                } else if (i == 4) {
                    left = 42 * unitWidth;
                } else {
                    left += fontBitmap.getWidth();
                }
                canvas.drawBitmap(fontBitmap, left, bitmap.getHeight() * 0.78f, paint);
            }
        }
        canvas.save();
        return bitmap;
    }
}
