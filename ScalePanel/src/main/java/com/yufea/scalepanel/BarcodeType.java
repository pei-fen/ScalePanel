package com.yufea.scalepanel;

/**
 * @author peifen
 * @date 2020/6/8 16:35
 */
public class BarcodeType {
    /**
     * Aztec 2D barcode format.
     */
    public static final String AZTEC = "AZTEC";

    /**
     * CODABAR 1D format.
     */
    public static final String CODABAR = "CODABAR";

    /**
     * Code 39 1D format.
     */
    public static final String CODE_39 = "CODE_39";

    /**
     * Code 93 1D format.
     */
    public static final String CODE_93 = "CODE_93";

    /**
     * Code 128 1D format.
     */
    public static final String CODE_128 = "CODE_128";

    /**
     * Data Matrix 2D barcode format.
     */
    public static final String DATA_MATRIX = "DATA_MATRIX";

    /**
     * EAN-8 1D format.
     */
    public static final String EAN_8 = "EAN_8";

    /**
     * EAN-13 1D format.
     */
    public static final String EAN_13 = "EAN_13";

    /**
     * ITF (Interleaved Two of Five) 1D format.
     */
    public static final String ITF = "ITF";

    /**
     * MaxiCode 2D barcode format.
     */
    public static final String MAXICODE = "MAXICODE";

    /**
     * PDF417 format.
     */
    public static final String PDF_417 = "PDF_417";

    /**
     * QR Code 2D barcode format.
     */
    public static final String QR_CODE = "QR_CODE";

    /**
     * RSS 14
     */
    public static final String RSS_14 = "RSS_14";

    /**
     * RSS EXPANDED
     */
    public static final String RSS_EXPANDED = "RSS_EXPANDED";

    /**
     * UPC-A 1D format.
     */
    public static final String UPC_A = "UPC_A";

    /**
     * UPC-E 1D format.
     */
    public static final String UPC_E = "UPC_E";

    /**
     * UPC/EAN extension format. Not a stand-alone format.
     */
    public static final String UPC_EAN_EXTENSION = "UPC_EAN_EXTENSION";
}
