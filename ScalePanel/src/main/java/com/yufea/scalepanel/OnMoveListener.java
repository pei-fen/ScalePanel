package com.yufea.scalepanel;

import com.yufea.scalepanel.view.ContentView;

/**
 * @author peifen
 * @date 2020/6/22 19:49
 */
public interface OnMoveListener {
    void onMove(ContentView contentView);
}
