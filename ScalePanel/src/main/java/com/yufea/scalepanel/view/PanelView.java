package com.yufea.scalepanel.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.yufea.scalepanel.config.PanelConfig;


/**
 * 表格面板View
 *
 * @author peifen
 * @date 2020/5/14 17:29
 */
public class PanelView extends View {

    private static final String TAG = PanelView.class.getSimpleName();
    private Paint mPaint;
    private PanelConfig mConfig = new PanelConfig();

    public PanelView(Context context) {
        super(context);
        initPaint();
    }

    public PanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public PanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PanelView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initPaint();
    }

    /**
     * 设置面板的属性配置
     *
     * @param config
     */
    public void setConfig(PanelConfig config) {
        mConfig = config;
    }

    /**
     * 获取面板属性配置
     *
     * @return
     */
    public PanelConfig getConfig() {
        return mConfig;
    }

    private void initPaint() {
        mPaint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Typeface typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD);
        mPaint.setTypeface(typeface);
        //设置画布背景颜色
        canvas.drawColor(mConfig.panelBackgroundColor);
        mPaint.setTextSize(mConfig.textSize);
        mPaint.setStrokeWidth(mConfig.gridStrokeWidth);
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        if (!mConfig.isFixedAxes) {
            //画垂直和水平的标尺背景
            mPaint.setColor(mConfig.barColor);
            canvas.drawRect(0, mConfig.markerBarXHeight, mConfig.markerBarYWidth, height, mPaint);//Y轴
            canvas.drawRect(mConfig.markerBarYWidth, 0, width, mConfig.markerBarXHeight, mPaint);//X轴
        }

        //绘制X轴和Y轴坐标
        int xLineCount = (int) ((width - mConfig.markerBarYWidth) / mConfig.gridCellWidth);
        float xWidth = mConfig.gridStrokeWidth;
        for (int i = 0; i <= xLineCount; i++) {//画x轴表格线 竖
            mPaint.setColor(mConfig.gridColor);
            int size = i;
            if (mConfig.unitText.equalsIgnoreCase("px")) {
                size = (int) (i * mConfig.gridCellHeight);
            }
            if (i == 0) {
                canvas.drawLine(mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight - 10, mConfig.markerBarYWidth + xWidth, height, mPaint);
            } else if (i % 10 == 0) {
                canvas.drawLine(mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight - 10, mConfig.markerBarYWidth + xWidth, height, mPaint);
                if (!mConfig.isFixedAxes) {
                    mPaint.setColor(mConfig.textColor);
                    canvas.drawText(size + "", mConfig.markerBarYWidth + xWidth - mPaint.measureText(size + "") / 2, mConfig.markerBarXHeight - 12, mPaint);
                }
            } else {
                canvas.drawLine(mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight - 5, mConfig.markerBarYWidth + xWidth, height, mPaint);
            }
            xWidth += mConfig.gridCellWidth;
        }
        int yLineCount = (int) ((height - mConfig.markerBarXHeight) / mConfig.gridCellHeight);
        float yHeight = mConfig.gridStrokeWidth;
        for (int i = 0; i <= yLineCount; i++) {//画y轴表格线 横
            mPaint.setColor(mConfig.gridColor);
            int size = i;
            if (mConfig.unitText.equalsIgnoreCase("px")) {
                size = (int) (i * mConfig.gridCellHeight);
            }
            if (i == 0) {
                canvas.drawLine(mConfig.markerBarYWidth - 10, mConfig.markerBarXHeight + yHeight, width, mConfig.markerBarXHeight + yHeight, mPaint);
            } else if (i % 10 == 0) {
                canvas.drawLine(mConfig.markerBarYWidth - 10, mConfig.markerBarXHeight + yHeight, width, mConfig.markerBarXHeight + yHeight, mPaint);
                if (!mConfig.isFixedAxes) {
                    float fontWidth2 = mPaint.measureText("" + size);
                    float descent2 = mPaint.getFontMetrics().descent;
                    float ascent2 = mPaint.getFontMetrics().ascent;
                    float fontHeight2 = descent2 - ascent2;
                    mPaint.setColor(mConfig.textColor);
                    canvas.drawText(size + "", mConfig.markerBarYWidth - 12 - fontWidth2, mConfig.markerBarXHeight + yHeight + fontHeight2 / 2 - fontHeight2 * 0.2f, mPaint);
                }
            } else {
                canvas.drawLine(mConfig.markerBarYWidth - 5, mConfig.markerBarYWidth + yHeight, width, mConfig.markerBarXHeight + yHeight, mPaint);
            }
            yHeight += mConfig.gridCellHeight;
        }

        //绘制中间的
        //画装有“mm”的蓝色的小框
        mPaint.setColor(mConfig.unitBackgroundColor);
        mPaint.setTextSize(mConfig.unitTextSize);
        canvas.drawRect(0, 0, mConfig.markerBarYWidth, mConfig.markerBarXHeight, mPaint);
        //设置画笔颜色
        mPaint.setColor(mConfig.unitTextColor);
        float fontWidth = mPaint.measureText(mConfig.unitText);
        float descent = mPaint.getFontMetrics().descent;
        float ascent = mPaint.getFontMetrics().ascent;
        float fontHeight = descent - ascent;
        canvas.drawText(mConfig.unitText, (mConfig.markerBarYWidth - fontWidth) / 2, (mConfig.markerBarXHeight + fontHeight) / 2 - mPaint.getFontMetrics().descent, mPaint);

        //绘制红色的分割线，一个分割线代表一个打印头的高度
        if (mConfig.printHeadHeight > 0) {
            int halveCount = (int) ((height - mConfig.markerBarXHeight) / mConfig.printHeadHeight);
            float halveHeight = mConfig.gridStrokeWidth;
            mPaint.setColor(Color.argb(50, 255, 0, 0));
            for (int i = 0; i <= halveCount; i++) {
                halveHeight += mConfig.printHeadHeight;
                canvas.drawLine(mConfig.markerBarYWidth, mConfig.markerBarXHeight + halveHeight, width, mConfig.markerBarXHeight + halveHeight, mPaint);
            }
        }
    }
}
