package com.yufea.scalepanel.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;

import com.yufea.scalepanel.config.PanelConfig;

/**
 * 轴线View
 *
 * @author peifen
 * @date 2020/5/14 17:29
 */
public class AxisView extends View {

    public static final int X_TYPE = 1; //绘制X轴
    public static final int Y_TYPE = 2; //绘制Y轴
    public static final int RECT_TYPE = 3;//绘制中间的矩形
    private static final String TAG = AxisView.class.getSimpleName();
    private Paint mPaint;
    private PanelConfig mConfig;
    private int mType = -1;
    private RelativeLayout mContainerView;

    public AxisView(Context context, int type) {
        super(context);
        mType = type;
        initPaint();
    }

    public AxisView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public AxisView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AxisView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initPaint();
    }

    private void initPaint() {
        mPaint = new Paint();
        mPaint.setStrokeWidth(2);
    }

    public void setConfig(PanelConfig config) {
        mConfig = config;
    }

    /**
     * 设置父容器
     *
     * @param containerView
     */
    public void setContainerView(RelativeLayout containerView) {
        mContainerView = containerView;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //设置画布背景颜色，稍暗的白色
//        canvas.drawRGB(236, 236, 236);
        canvas.drawARGB(0, 0, 0, 0);
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        mPaint.setTextSize(mConfig.textSize);
        mPaint.setStrokeWidth(mConfig.gridStrokeWidth);
        if (mType == X_TYPE) { //如果宽度大于高度，则认为是画X轴
            //画垂直和水平的标尺背景，中度的黄色
            mPaint.setColor(mConfig.barColor);
            canvas.drawRect(mConfig.markerBarYWidth, 0, width, height, mPaint);//X轴
            int xLineCount = (int) ((width - mConfig.markerBarYWidth) / mConfig.gridCellWidth);
            float xWidth = mConfig.gridStrokeWidth;
            mPaint.setTextSize(mConfig.textSize);
            for (int i = 0; i <= xLineCount; i++) {//画x轴表格线
                mPaint.setColor(mConfig.scaleColor);
                int size = i;
                if (mConfig.unitText.equalsIgnoreCase("px")) {
                    size = (int) (i * mConfig.gridCellWidth);
                }
                if (i == 0) {
                    canvas.drawLine(mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight - mConfig.scaleSize * 2, mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight, mPaint);
                } else if (i % 10 == 0) {
                    canvas.drawLine(mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight - mConfig.scaleSize * 2, mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight, mPaint);
                    mPaint.setColor(mConfig.textColor);
                    canvas.drawText(size + "", mConfig.markerBarYWidth + xWidth - mPaint.measureText(size + "") / 2, mConfig.markerBarXHeight - mConfig.scaleSize * 2.5f, mPaint);
                } else {
                    canvas.drawLine(mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight - mConfig.scaleSize, mConfig.markerBarYWidth + xWidth, mConfig.markerBarXHeight, mPaint);
                }
                xWidth += mConfig.gridCellWidth;
            }
        } else if (mType == Y_TYPE) { //如果高度大于宽度，则认为是画Y轴
            //画垂直和水平的标尺背景，中度的黄色
            mPaint.setColor(mConfig.barColor);
            canvas.drawRect(0, mConfig.markerBarXHeight, mConfig.markerBarYWidth, height, mPaint);//Y轴
            int yLineCount = (int) ((height - mConfig.markerBarXHeight) / mConfig.gridCellHeight);
            float yHeight = mConfig.gridStrokeWidth;
            mPaint.setTextSize(mConfig.textSize);
            for (int i = 0; i <= yLineCount; i++) {//画y轴表格线
                mPaint.setColor(mConfig.scaleColor);
                int size = i;
                if (mConfig.unitText.equalsIgnoreCase("px")) {
                    size = (int) (i * mConfig.gridCellHeight);
                }
                if (i == 0) {
                    canvas.drawLine(mConfig.markerBarYWidth - mConfig.scaleSize * 2, mConfig.markerBarXHeight + yHeight, mConfig.markerBarYWidth, mConfig.markerBarXHeight + yHeight, mPaint);
                } else if (i % 10 == 0) {
                    canvas.drawLine(mConfig.markerBarYWidth - mConfig.scaleSize * 2, mConfig.markerBarXHeight + yHeight, mConfig.markerBarYWidth, mConfig.markerBarXHeight + yHeight, mPaint);
                    float fontWidth2 = mPaint.measureText("" + size);
                    float descent2 = mPaint.getFontMetrics().descent;
                    float ascent2 = mPaint.getFontMetrics().ascent;
                    float fontHeight2 = descent2 - ascent2;
                    mPaint.setColor(mConfig.textColor);
                    canvas.drawText(size + "", mConfig.markerBarYWidth - mConfig.scaleSize * 2.5f - fontWidth2, mConfig.markerBarXHeight + yHeight + fontHeight2 / 2 - fontHeight2 * 0.2f, mPaint);
                } else {
                    canvas.drawLine(mConfig.markerBarYWidth - mConfig.scaleSize, mConfig.markerBarYWidth + yHeight, mConfig.markerBarYWidth, mConfig.markerBarXHeight + yHeight, mPaint);
                }
                yHeight += mConfig.gridCellHeight;
            }
        } else if (mType == RECT_TYPE) {//绘制中间的蓝色矩形
            mPaint.setColor(mConfig.unitBackgroundColor);
            mPaint.setTextSize(mConfig.unitTextSize);
            canvas.drawRect(0, 0, mConfig.markerBarYWidth, mConfig.markerBarXHeight, mPaint);
            //设置画笔颜色
            mPaint.setColor(mConfig.unitTextColor);
            float fontWidth = mPaint.measureText(mConfig.unitText);
            float descent = mPaint.getFontMetrics().descent;
            float ascent = mPaint.getFontMetrics().ascent;
            float fontHeight = descent - ascent;
            canvas.drawText(mConfig.unitText, (mConfig.markerBarYWidth - fontWidth) / 2, (mConfig.markerBarXHeight + fontHeight) / 2 - mPaint.getFontMetrics().descent, mPaint);
        }

    }
}
