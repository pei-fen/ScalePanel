package com.yufea.scalepanel.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.yufea.scalepanel.config.BarCodeConfig;
import com.yufea.scalepanel.config.ContentConfig;
import com.yufea.scalepanel.config.CountConfig;
import com.yufea.scalepanel.config.DateConfig;
import com.yufea.scalepanel.config.ImageConfig;
import com.yufea.scalepanel.config.QRCodeConfig;
import com.yufea.scalepanel.config.TextConfig;
import com.yufea.scalepanel.utils.BitmapUtil;
import com.yufea.scalepanel.utils.CommonUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * 内容View
 *
 * @author peifen
 * @date 2020/5/14 17:29
 */
@SuppressLint("AppCompatCustomView")
public class ContentView extends ImageView {

    private static final String TAG = ContentView.class.getSimpleName();
    private Paint mPaint;
    public float mLocationX = 0;  //控件位置X轴坐标，包含了markerBarYWidth 宽度
    public float mLocationY = 0;  //控件位置Y轴坐标，包含了markerBarXHeight 高度
    private PanelView mPanelView;
    private ContentConfig mConfig;
    private boolean isShowBorder = false; //是否显示边框
    private List<OnEventListener> mOnEventListeners = new ArrayList<>();

    public boolean isShowBorder() {
        return isShowBorder;
    }

    public void setShowBorder(boolean showBorder) {
        isShowBorder = showBorder;
    }

    public ContentView(Context context) {
        super(context);
        initPaint();
    }

    public ContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public ContentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    public void setPancelView(PanelView panelView) {
        mPanelView = panelView;
        mLocationX = mPanelView.getConfig().markerBarYWidth;
        mLocationY = mPanelView.getConfig().markerBarXHeight;
    }

    public ContentConfig getConfig() {
        return mConfig;
    }

    private void initPaint() {
        mPaint = new Paint();
    }

    private Bitmap mBitmap;

    /**
     * 设置配置文件
     *
     * @param config
     */
    public void setConfig(ContentConfig config) {
        mConfig = config;
        mLocationX = mPanelView.getConfig().markerBarYWidth + config.x * config.scale;
        mLocationY = mPanelView.getConfig().markerBarXHeight + config.y * config.scale;
        if (config instanceof BarCodeConfig) {
            mBitmap = BitmapUtil.createBarCode((BarCodeConfig) config);
        } else if (config instanceof QRCodeConfig) {
            mBitmap = BitmapUtil.createQrCode((QRCodeConfig) config);
        } else if (config instanceof ImageConfig) {
            mBitmap = BitmapUtil.createBitmap((ImageConfig) config);
        } else if (config instanceof DateConfig) {
            TextConfig tempConfig = convertToTextConfig((DateConfig) config);
            mBitmap = BitmapUtil.convertToBitmapByWidthAndHeight(tempConfig);
        } else if (config instanceof CountConfig) {
            TextConfig tempConfig = convertToTextConfig((CountConfig) config);
            mBitmap = BitmapUtil.convertToBitmapByWidthAndHeight(tempConfig);
        } else if (config instanceof TextConfig) {
            mBitmap = BitmapUtil.convertToBitmapByWidthAndHeight((TextConfig) config);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.width = mBitmap.getWidth();
        layoutParams.height = mBitmap.getHeight();
        layoutParams.setMargins((int) mLocationX, (int) mLocationY, 0, 0);
        setLayoutParams(layoutParams);
        invalidate();
    }

    public TextConfig convertToTextConfig(DateConfig dateConfig) {
        TextConfig textConfig = new TextConfig();
        textConfig.contentType = dateConfig.contentType;
        textConfig.scale = dateConfig.scale;
        textConfig.background = dateConfig.background;
        textConfig.borderColor = dateConfig.borderColor;
        textConfig.borderSize = dateConfig.borderSize;
        textConfig.directionDegress = dateConfig.directionDegress;
        textConfig.directionOffsetX = dateConfig.directionOffsetX;
        textConfig.directionOffsetY = dateConfig.directionOffsetY;
        textConfig.width = dateConfig.width;
        textConfig.isFixedWidth = dateConfig.isFixedWidth;
        textConfig.height = dateConfig.height;
        textConfig.x = dateConfig.x;
        textConfig.y = dateConfig.y;
        textConfig.space = dateConfig.space;
        textConfig.marginTop = dateConfig.marginTop;
        textConfig.marginLeft = dateConfig.marginLeft;
        textConfig.heightExtend = dateConfig.heightExtend;
        textConfig.offsetTop = dateConfig.offsetTop;
        textConfig.offsetLeft = dateConfig.offsetLeft;
        textConfig.offsetRight = dateConfig.offsetRight;
        textConfig.offsetBottom = dateConfig.offsetBottom;
        StringBuilder textSb = new StringBuilder();
        if (dateConfig.isDisplayDate) {
            textSb.append(dateConfig.dateText);
            if (dateConfig.isDisplayTime && CommonUtil.getDateFlag(dateConfig.dateFormat) != null) {
                //判断时不时自定义格式
                textSb.append(" ");
            }
        }
        if (dateConfig.isDisplayTime) {
            textSb.append(dateConfig.timeText);
//            textSb.append(TextUtils.isEmpty(dateConfig.timeSymbols[2]) ? "" : dateConfig.timeSymbols[2]);
        }

        textConfig.text = textSb.toString();
        textConfig.textColor = dateConfig.textColor;
        textConfig.textSize = dateConfig.textSize;
        textConfig.fontFamily = dateConfig.fontFamily;
        textConfig.fontStyle = dateConfig.fontStyle;
        return textConfig;
    }

    public TextConfig convertToTextConfig(CountConfig countConfig) {
        TextConfig textConfig = new TextConfig();
        textConfig.contentType = countConfig.contentType;
        textConfig.scale = countConfig.scale;
        textConfig.background = countConfig.background;
        textConfig.borderColor = countConfig.borderColor;
        textConfig.borderSize = countConfig.borderSize;
        textConfig.directionDegress = countConfig.directionDegress;
        textConfig.directionOffsetX = countConfig.directionOffsetX;
        textConfig.directionOffsetY = countConfig.directionOffsetY;
        textConfig.width = countConfig.width;
        textConfig.isFixedWidth = countConfig.isFixedWidth;
        textConfig.height = countConfig.height;
        textConfig.x = countConfig.x;
        textConfig.y = countConfig.y;
        textConfig.space = countConfig.space;
        textConfig.marginTop = countConfig.marginTop;
        textConfig.marginLeft = countConfig.marginLeft;
        textConfig.heightExtend = countConfig.heightExtend;
        textConfig.offsetTop = countConfig.offsetTop;
        textConfig.offsetLeft = countConfig.offsetLeft;
        textConfig.offsetRight = countConfig.offsetRight;
        textConfig.offsetBottom = countConfig.offsetBottom;
        long currentValue = countConfig.startValue;
        if (countConfig.currentCount != null) {
            currentValue = countConfig.currentCount;
        }
        textConfig.text = formatParam(currentValue, countConfig.digit, countConfig.isShowZero);
        textConfig.textColor = countConfig.textColor;
        textConfig.textSize = countConfig.textSize;
        textConfig.fontFamily = countConfig.fontFamily;
        textConfig.fontStyle = countConfig.fontStyle;
        return textConfig;
    }

    private String formatParam(long value, int digits, boolean isShowZero) {
        String s = String.valueOf(value);
        int initLen = s.length();
        if (initLen > digits) {
            //直接裁减掉
            s = s.substring(initLen - digits, initLen);
        }
        if (!isShowZero) {
            for (int i = 0; i < digits - initLen; i++) {
                s = " "+s;
            }
            return s;
        }
        if (initLen < digits) {
            for (int i = 0; i < digits - initLen; i++) {
                s = "0" + s;
            }
        }
        return s;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    /**
     * 设置事件监听
     *
     * @param onEventListener
     */
    public void addOnEventListener(OnEventListener onEventListener) {
        mOnEventListeners.add(onEventListener);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(mConfig.background);
        canvas.drawBitmap(mBitmap, 0, 0, mPaint);
        if (isShowBorder) {
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(mConfig.borderSize);
            mPaint.setColor(mConfig.borderColor);
            canvas.drawRect(0, 0, mBitmap.getWidth(), mBitmap.getHeight(), mPaint);
        }
    }

    /**
     * 设置是否开启触摸移动控件
     *
     * @param isEnable 是否开启
     */
    public void setTouchMove(boolean isEnable) {
        if (isEnable) {
            setOnTouchListener(new OnTouchListener() {
                float moveX = 0;
                float moveY = 0;
                float barHeight = 0;
                float barWidth = 0;
                long startTime = 0;
                float startRawX;
                float startRawY;

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            moveX = motionEvent.getX();//按下去后，手在控件中x轴的位置
                            moveY = motionEvent.getY(); //按下去后，手在控件中y轴的位置
                            startRawX = motionEvent.getRawX(); //按下去后，手在屏幕x轴的位置
                            startRawY = motionEvent.getRawY(); //按下去后，手在屏幕y轴的位置

                            barHeight = startRawY - mLocationY - moveY;
                            barWidth = startRawX - mLocationX - moveX;
                            Log.i(TAG, "按下：" + moveX + "," + moveY);
                            startTime = System.currentTimeMillis();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            float rawX = motionEvent.getRawX();
                            float rawY = motionEvent.getRawY();
                            float tempLocationX = rawX - moveX - barWidth;
                            float tempLocationY = rawY - moveY - barHeight;

                            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
                            float widthC = mPanelView.getConfig().widht - mPanelView.getConfig().markerBarYWidth - lp.width;
                            if (widthC >= 0) {
                                if (tempLocationX <= mPanelView.getConfig().markerBarYWidth - mConfig.offsetLeft) {
                                    mLocationX = mPanelView.getConfig().markerBarYWidth - mConfig.offsetLeft;
                                } else if (tempLocationX >= mPanelView.getConfig().widht - lp.width + mConfig.offsetRight) {
                                    mLocationX = mPanelView.getConfig().widht - lp.width + mConfig.offsetRight;
                                } else {
                                    mLocationX = tempLocationX;
                                }
                            } else {//处理当前控件宽度比面板宽度宽的情况
                                if (tempLocationX < widthC + mPanelView.getConfig().markerBarYWidth) {
                                    mLocationX = widthC + mPanelView.getConfig().markerBarYWidth;
                                } else if (tempLocationX > mPanelView.getConfig().markerBarYWidth) {
                                    mLocationX = mPanelView.getConfig().markerBarYWidth;
                                } else {
                                    mLocationX = tempLocationX;
                                }
                            }
                            float heightC = mPanelView.getConfig().height - mPanelView.getConfig().markerBarXHeight - lp.height;
                            if (heightC >= 0) {
                                if (tempLocationY <= mPanelView.getConfig().markerBarXHeight - mConfig.offsetTop) {
                                    mLocationY = mPanelView.getConfig().markerBarXHeight - mConfig.offsetTop;
                                } else if (tempLocationY >= mPanelView.getConfig().height - lp.height + mConfig.offsetBottom) {
                                    mLocationY = mPanelView.getConfig().height - lp.height + mConfig.offsetBottom;
                                } else {
                                    mLocationY = tempLocationY;
                                }
                            } else {//处理当前控件高度比面板高度高的情况
                                if (tempLocationY < heightC + mPanelView.getConfig().markerBarXHeight) {
                                    mLocationY = heightC + mPanelView.getConfig().markerBarXHeight;
                                } else if (tempLocationY > mPanelView.getConfig().markerBarXHeight) {
                                    mLocationY = mPanelView.getConfig().markerBarXHeight;
                                } else {
                                    mLocationY = tempLocationY;
                                }
                            }

                            lp.setMargins((int) mLocationX, (int) mLocationY, 0, 0);
                            setLayoutParams(lp);
                            mConfig.x = (mLocationX - mPanelView.getConfig().markerBarYWidth) / mConfig.scale;
                            mConfig.y = (mLocationY - mPanelView.getConfig().markerBarXHeight) / mConfig.scale;
                            break;
                        case MotionEvent.ACTION_UP:
                            Log.i(TAG, "放开");
                            long endTime = System.currentTimeMillis();
                            float endRawX = motionEvent.getRawX();
                            float endRawY = motionEvent.getRawY();

                            //如果按下去到放开的时间小于0.5秒，且控件移动的像素x轴和y轴都小于2，则认为是点击事件
                            if (endTime - startTime < 0.5 * 1000 && Math.abs(endRawX - startRawX) < 2 && Math.abs(endRawY - startRawY) < 2) {
                                for (OnEventListener onEventListener : mOnEventListeners) {
                                    onEventListener.onClick(view);
                                }
                            }
                            break;

                    }
                    for (OnEventListener onEventListener : mOnEventListeners) {
                        onEventListener.onTouch(view, motionEvent);
                    }
                    return true;
                }
            });
        } else {
            setOnTouchListener(null);
        }
    }

    public interface OnEventListener {
        void onTouch(View view, MotionEvent motionEvent);

        void onClick(View view);
    }
}
