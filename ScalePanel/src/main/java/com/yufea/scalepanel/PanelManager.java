package com.yufea.scalepanel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.yufea.scalepanel.config.ContentConfig;
import com.yufea.scalepanel.config.PanelConfig;
import com.yufea.scalepanel.view.AxisView;
import com.yufea.scalepanel.view.ContentView;
import com.yufea.scalepanel.view.PanelView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author peifen
 * @date 2020/5/15 14:10
 */
public class PanelManager {

    private static final String TAG = PanelManager.class.getSimpleName();
    private final RelativeLayout mContainerView; //容器
    private RelativeLayout mCanvasView; //存放所有内容的画布容器，包括表格和内容
    private AxisView mAxisXView;//X轴
    private AxisView mAxisYView;//Y轴
    private boolean isFixedAxes = false; //是否固定x和y轴
    private Context mContext;
    private final List<ContentView> mContentViewList = new ArrayList<>();

    private PanelView mPanelView;

    private ContentView mSelectedContentView;

    private View.OnClickListener mPanelOnClickListener;

    private PanelManager(Context context, RelativeLayout container) {
        this.mContext = context;
        this.mContainerView = container;
    }

    /**
     * 设置容器画布的点击事件
     *
     * @param listener 点击监听
     */
    public void setPanelOnClickListener(View.OnClickListener listener) {
        mPanelOnClickListener = listener;
    }

    /**
     * 创建一个实例PannelManager
     *
     * @param context   上下文
     * @param container 容器View
     * @return
     */
    public static PanelManager newInstance(Context context, RelativeLayout container) {
        return new PanelManager(context, container);
    }

    /**
     * 绘制仪表盘
     *
     * @param config      参数配置
     * @param isFixedAxes 是否固定坐标轴x和y
     * @return
     */
    public void drawPanel(final PanelConfig config, boolean isFixedAxes) {
        this.isFixedAxes = isFixedAxes;
        config.isFixedAxes = isFixedAxes;
        int containerWidth = mContainerView.getMeasuredWidth();
        int containerHeight = mContainerView.getMeasuredHeight();
        mCanvasView = new RelativeLayout(mContext);
        mContainerView.addView(mCanvasView);
        ViewGroup.LayoutParams canvasViewLp = mCanvasView.getLayoutParams();
        canvasViewLp.height = (int) config.height;
        canvasViewLp.width = (int) config.widht;
        mCanvasView.setLayoutParams(canvasViewLp);
        //绘制表格面板
        PanelView panelView = new PanelView(mContext);
        mCanvasView.addView(panelView);
        panelView.setConfig(config);
        ViewGroup.LayoutParams layoutParams = panelView.getLayoutParams();
        layoutParams.height = (int) config.height;
        layoutParams.width = (int) config.widht;
        panelView.setLayoutParams(layoutParams);
        panelView.invalidate();
        mPanelView = panelView;

        if (PanelManager.this.isFixedAxes) {
            //绘制X轴
            mAxisXView = new AxisView(mContext, AxisView.X_TYPE);
            mContainerView.addView(mAxisXView);
            mAxisXView.setConfig(config);
            ViewGroup.LayoutParams axisXViewLp = mAxisXView.getLayoutParams();
            axisXViewLp.height = (int) config.markerBarXHeight;
            if (containerWidth > config.widht) {
                axisXViewLp.width = containerWidth;
            } else {
                axisXViewLp.width = (int) config.widht;
            }
            mAxisXView.invalidate();

            //绘制Y轴
            mAxisYView = new AxisView(mContext, AxisView.Y_TYPE);
            mContainerView.addView(mAxisYView);
            mAxisYView.setConfig(config);
            ViewGroup.LayoutParams axisYViewLp = mAxisYView.getLayoutParams();
            if (containerHeight > config.height) {
                axisYViewLp.height = containerHeight;
            } else {
                axisYViewLp.height = (int) config.height;
            }
            axisYViewLp.width = (int) config.markerBarYWidth;
            mAxisYView.invalidate();

            //绘制中间的矩形
            AxisView rectView = new AxisView(mContext, AxisView.RECT_TYPE);
            mContainerView.addView(rectView);
            rectView.setConfig(config);
            ViewGroup.LayoutParams rectViewLp = rectView.getLayoutParams();
            rectViewLp.height = (int) config.markerBarXHeight;
            rectViewLp.width = (int) config.markerBarYWidth;
            rectView.invalidate();
        }
        openTouchListener();
    }

    public void updatePanel(PanelConfig config) {
        mCanvasView.removeAllViews();
        mContainerView.removeAllViews();
        int containerWidth = mContainerView.getMeasuredWidth();
        int containerHeight = mContainerView.getMeasuredHeight();
        mCanvasView = new RelativeLayout(mContext);
        mContainerView.addView(mCanvasView);
        ViewGroup.LayoutParams canvasViewLp = mCanvasView.getLayoutParams();
        canvasViewLp.height = (int) config.height;
        canvasViewLp.width = (int) config.widht;
        mCanvasView.setLayoutParams(canvasViewLp);
        //绘制表格面板
        PanelView panelView = new PanelView(mContext);
        mCanvasView.addView(panelView);
        panelView.setConfig(config);
        ViewGroup.LayoutParams layoutParams = panelView.getLayoutParams();
        layoutParams.height = (int) config.height;
        layoutParams.width = (int) config.widht;
        panelView.setLayoutParams(layoutParams);
        panelView.invalidate();
        mPanelView = panelView;

        if (PanelManager.this.isFixedAxes) {
            //绘制X轴
            mAxisXView = new AxisView(mContext, AxisView.X_TYPE);
            mContainerView.addView(mAxisXView);
            mAxisXView.setConfig(config);
            ViewGroup.LayoutParams axisXViewLp = mAxisXView.getLayoutParams();
            axisXViewLp.height = (int) config.markerBarXHeight;
            if (containerWidth > config.widht) {
                axisXViewLp.width = containerWidth;
            } else {
                axisXViewLp.width = (int) config.widht;
            }
            mAxisXView.invalidate();

            //绘制Y轴
            mAxisYView = new AxisView(mContext, AxisView.Y_TYPE);
            mContainerView.addView(mAxisYView);
            mAxisYView.setConfig(config);
            ViewGroup.LayoutParams axisYViewLp = mAxisYView.getLayoutParams();
            if (containerHeight > config.height) {
                axisYViewLp.height = containerHeight;
            } else {
                axisYViewLp.height = (int) config.height;
            }
            axisYViewLp.width = (int) config.markerBarYWidth;
            mAxisYView.invalidate();

            //绘制中间的矩形
            AxisView rectView = new AxisView(mContext, AxisView.RECT_TYPE);
            mContainerView.addView(rectView);
            rectView.setConfig(config);
            ViewGroup.LayoutParams rectViewLp = rectView.getLayoutParams();
            rectViewLp.height = (int) config.markerBarXHeight;
            rectViewLp.width = (int) config.markerBarYWidth;
            rectView.invalidate();
        }
        openTouchListener();
    }

    /**
     * 添加内容
     *
     * @param config 内容配置信息
     * @see com.yufea.scalepanel.config.TextConfig
     * @see com.yufea.scalepanel.config.ImageConfig
     * @see com.yufea.scalepanel.config.QRCodeConfig
     * @see com.yufea.scalepanel.config.BarCodeConfig
     */
    public ContentView addContentView(ContentConfig config) {
        return addContentView(config, true);
    }

    public ContentView addContentView(ContentConfig config, boolean isTouchMove) {
        ContentView contentView = new ContentView(mContext);
        contentView.setPancelView(mPanelView);
        contentView.setConfig(config);
        contentView.addOnEventListener(new ContentView.OnEventListener() {
            @Override
            public void onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        updateContentViewLayer((ContentView) view);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:

                        break;
                }
            }

            @Override
            public void onClick(View view) {
                clearAllContentViewBorder();
                setContentViewBorder((ContentView) view);
            }
        });
        contentView.setTouchMove(isTouchMove);
        contentView.invalidate();
        mCanvasView.addView(contentView);
        mContentViewList.add(contentView);
        return contentView;
    }

    public void updateContentView(float scale) {
        for (ContentView contentView : mContentViewList) {
            contentView.setPancelView(mPanelView);
            ContentConfig config = contentView.getConfig();
            config.scale = scale;
            contentView.setConfig(config);
            contentView.invalidate();
            mCanvasView.addView(contentView);
        }
    }

    /**
     * 更新内容层级，当前点击后的内容为最顶层
     *
     * @param contentView contentView
     */
    public void updateContentViewLayer(ContentView contentView) {
        mCanvasView.removeView(contentView);
        mCanvasView.addView(contentView);
    }

    /**
     * 清除所有contentView的边框
     */
    public void clearAllContentViewBorder() {
        for (ContentView v : mContentViewList) {
            v.setShowBorder(false);
            v.invalidate();
        }
        mSelectedContentView = null;
    }

    /**
     * 移除所有内容控件
     */
    public void deleteAllContentView() {
        for (ContentView contentView : mContentViewList) {
            mCanvasView.removeView(contentView);
        }
        mSelectedContentView = null;
        mContentViewList.clear();
    }

    /**
     * 获取选中的contentView
     *
     * @return 选中的ContentView
     */
    public ContentView getSelectedContentView() {
        return mSelectedContentView;
    }

    /**
     * 获取所有的contentView
     *
     * @return
     */
    public List<ContentView> getAllContentView() {
        return mContentViewList;
    }

    /**
     * 移除内容控件
     *
     * @param contentView contentView
     * @return 是否移除成功
     */
    public boolean deleteContentView(ContentView contentView) {
        if (contentView != null) {
            mCanvasView.removeView(contentView);
            mContentViewList.remove(contentView);
            if (contentView == mSelectedContentView) {
                mSelectedContentView = null;
            }
            return true;
        }
        return false;
    }

    /**
     * 设置contentView的边框
     *
     * @param contentView contentView
     */
    public void setContentViewBorder(ContentView contentView) {
        mSelectedContentView = contentView;
        contentView.setShowBorder(true);
        contentView.invalidate();
    }

    /**
     * @param contentView
     */
    public void cancelContentViewBorder(ContentView contentView) {
        mSelectedContentView = contentView;
        contentView.setShowBorder(false);
        contentView.invalidate();
    }

    /**
     * 生成有效内容
     *
     * @return bitmap
     */
    public Bitmap generateValidBitmap(int maxHeight) {
        if (mContentViewList.size() == 0) {
            return null;
        }
        int width = getValidWidth();
        int validHeight = getValidHeight();
        maxHeight = Math.min(validHeight, maxHeight);
        Bitmap bitmap = Bitmap.createBitmap(width, maxHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        for (ContentView contentView : mContentViewList) {
            ContentConfig config = contentView.getConfig();
            canvas.drawBitmap(contentView.getBitmap(), config.x * config.scale, config.y * config.scale, null);
        }
        canvas.save();
        return bitmap;
    }

    private int getValidHeight() {
        int validHeight = 0;
        for (ContentView contentView : mContentViewList) {
            Bitmap bitmap = contentView.getBitmap();
            ContentConfig config = contentView.getConfig();
            if (bitmap.getHeight() + config.y * config.scale > validHeight) {
                validHeight = (int) (bitmap.getHeight() + config.y * config.scale);
            }
        }
        return validHeight;
    }

    /**
     * 获取有效的宽度（获取显示的内容中，最大的有效宽度）
     *
     * @return
     */
    private int getValidWidth() {
        int validWidth = 0;
        for (ContentView contentView : mContentViewList) {
            Bitmap bitmap = contentView.getBitmap();
            ContentConfig config = contentView.getConfig();
            if (bitmap.getWidth() + config.x * config.scale > validWidth) {
                validWidth = (int) (bitmap.getWidth() + config.x * config.scale);
            }
        }
        return validWidth;
    }


    /**
     * 移动到contentView位置
     *
     * @param contentView contentView
     */
    public void moveToContentView(ContentView contentView) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mCanvasView.getLayoutParams();

        float newLeftMargin = contentView.getConfig().x;
        float newTopMargin = contentView.getConfig().y;

        int widthOffset = mContainerView.getWidth() - lp.width;
        int heightOffset = mContainerView.getHeight() - lp.height;

        if (widthOffset >= 0) { //容器的宽度比画布大
            newLeftMargin = 0;
        } else if (contentView.getConfig().x + widthOffset > 0) { //画布超出没有显示 小于 内容的左边距
            newLeftMargin = -widthOffset;
        } else if (contentView.getConfig().x + widthOffset <= 0) {
            newLeftMargin = contentView.getConfig().x;
        }
        if (heightOffset >= 0) {
            newTopMargin = 0;
        } else if (contentView.getConfig().y + heightOffset > 0) {
            newTopMargin = -heightOffset;
        } else if (contentView.getConfig().y + heightOffset <= 0) {
            newTopMargin = contentView.getConfig().y;
        }
        lp.setMargins((int) -newLeftMargin, (int) -newTopMargin, 0, 0);
        mCanvasView.setLayoutParams(lp);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void openTouchListener() {
        mCanvasView.setOnTouchListener(new View.OnTouchListener() {
            float startRawX;
            float startRawY;
            int leftMargin = 0;
            int topMargin = 0;
            RelativeLayout.LayoutParams lp;
            RelativeLayout.LayoutParams axisXViewLp;
            RelativeLayout.LayoutParams axisYViewLp;
            int containerWidth = 0;
            int containerHeight = 0;
            int canvasWidth = 0;
            int canvasHeight = 0;
            long startTime = 0;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.i(TAG, "move..." + motionEvent.getAction() + ",x=" + motionEvent.getRawX() + ",y=" + motionEvent.getRawY());
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startRawX = motionEvent.getRawX();
                        startRawY = motionEvent.getRawY();
                        lp = (RelativeLayout.LayoutParams) mCanvasView.getLayoutParams();
                        leftMargin = lp.leftMargin;
                        topMargin = lp.topMargin;
                        canvasWidth = lp.width;
                        canvasHeight = lp.height;
                        containerWidth = mContainerView.getWidth();
                        containerHeight = mContainerView.getHeight();
                        if (isFixedAxes) {
                            axisXViewLp = (RelativeLayout.LayoutParams) mAxisXView.getLayoutParams();
                            axisYViewLp = (RelativeLayout.LayoutParams) mAxisYView.getLayoutParams();
                        }
                        startTime = System.currentTimeMillis();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float x = motionEvent.getRawX() - startRawX; //x偏移量
                        float y = motionEvent.getRawY() - startRawY; //y偏移量
                        int newLeftMargin = (int) x + leftMargin;
                        int newTopMargin = (int) y + topMargin;

                        int widthOffset = containerWidth - canvasWidth;
                        int heightOffset = containerHeight - canvasHeight;
//                        Log.i(TAG, "x = " + x + ",y=" + y + "  newLeftMargin = " + newLeftMargin + ",newTopMargin = " + newTopMargin + "  widthOffset=" + widthOffset + ",heightOffset=" + heightOffset);
                        if (widthOffset >= 0) {
                            newLeftMargin = 0;
                        } else if (newLeftMargin <= widthOffset) {
                            newLeftMargin = widthOffset;
                        } else if (newLeftMargin >= 0) {
                            newLeftMargin = 0;
                        }
                        if (heightOffset >= 0) {
                            newTopMargin = 0;
                        } else if (newTopMargin <= heightOffset) {
                            newTopMargin = heightOffset;
                        } else if (newTopMargin >= 0) {
                            newTopMargin = 0;
                        }
                        lp.setMargins(newLeftMargin, newTopMargin, 0, 0);
                        mCanvasView.setLayoutParams(lp);
                        if (isFixedAxes) {
                            axisXViewLp.leftMargin = newLeftMargin;
                            mAxisXView.setLayoutParams(axisXViewLp);
                            axisYViewLp.topMargin = newTopMargin;
                            mAxisYView.setLayoutParams(axisYViewLp);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        long endTime = System.currentTimeMillis();
                        float endRawX = motionEvent.getRawX();
                        float endRawY = motionEvent.getRawY();
                        //如果按下去到放开的时间小于0.5秒，且控件移动的像素x轴和y轴都小于2，则认为是点击事件
                        if (endTime - startTime < 0.5 * 1000 && Math.abs(endRawX - startRawX) < 2 && Math.abs(endRawY - startRawY) < 2) {
                            if (mPanelOnClickListener != null) {
                                mPanelOnClickListener.onClick(view);
                            }
                        }
                        break;
                }
                return true;
            }
        });
    }
}
