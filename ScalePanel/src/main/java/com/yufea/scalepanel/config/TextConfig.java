package com.yufea.scalepanel.config;

import android.graphics.Color;

/**
 * 文本属性
 *
 * @author peifen
 * @date 2020/5/18 12:56
 */
public class TextConfig extends ContentConfig {
    /**
     * 文本
     */
    public String text;
    /**
     * 字体颜色
     */
    public int textColor = Color.BLACK;
    /**
     * 字体大小，建议使用sp
     * <p>
     * 请使用height和widht
     */
    @Deprecated
    public int textSize = 50;
    /**
     * 字体
     */
    public String fontFamily;
    /**
     * 字体类型 参考 Typeface
     */
    public int fontStyle = 0;

    /**
     * 可变数据
     */
    public String[] variableDatas;
}
