package com.yufea.scalepanel.config;

import android.graphics.Color;

/**
 * 条码属性
 *
 * @author peifen
 * @date 2020/5/18 12:56
 */
public class BarCodeConfig extends ContentConfig {
    /**
     * 文本
     */
    public String text;
    /**
     * 字体颜色
     */
    public int textColor = Color.BLACK;

    /**
     * 编码类型
     */
    public String encodeType = "CODE128";

    /**
     * 是否显示文本
     */
    public boolean isDisplayText = false;

    /**
     * 文本宽度
     */
    @Deprecated
    public float textWidth;
    /**
     * 文本高度
     */
    @Deprecated
    public float textHeight = height * 0.22f;
    /**
     * 文本的对齐方式
     * 1：左对齐
     * 2：右对齐
     * 3：居中
     */
    public int textAlign = 3;

    /**
     * 可变数据
     */
    public String[] variableDatas;
}
