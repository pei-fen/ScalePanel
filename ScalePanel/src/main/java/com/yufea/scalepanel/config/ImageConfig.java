package com.yufea.scalepanel.config;

import android.graphics.Bitmap;

/**
 * 图片属性
 *
 * @author peifen
 * @date 2020/5/18 12:56
 */
public class ImageConfig extends ContentConfig {
    /**
     * 原始bitmap的像素集合
     */
//    public int[] originalPixels;
    public Bitmap originalBitmap;
    /**
     * 字模集合
     */
    public byte[] fontModels;

    /**
     * 原始图片宽度，这个值为最原始的图片宽度，与scale无关
     */
    public float originalImageWidth;//解决图片缩小后再放大失真

    /**
     * 原始图片的高度，这个值为最原始的图片高度，与scale无关
     */
    public float originalImageHeight;//解决图片缩小后再放大失真

    /**
     * 可变数据
     */
    public String[] variableDatas;
}
