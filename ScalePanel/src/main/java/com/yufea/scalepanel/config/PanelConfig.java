package com.yufea.scalepanel.config;

import android.graphics.Color;

/**
 * 面板的配置属性
 *
 * @author peifen
 * @date 2020/5/18 12:12
 */
public class PanelConfig {
    /**
     * 面板的宽度 单位：像素
     */
    public float widht = 1000;//面板的宽度
    /**
     * 面板的高度 单位：像素
     */
    public float height = 1000;//面板的高度
    /**
     * X轴标记栏的高度 单位：像素
     */
    public float markerBarXHeight = 40;//X轴标记栏的高度
    /**
     * Y轴标记栏的宽度 单位：像素
     */
    public float markerBarYWidth = 40;//Y轴标记栏的宽度
    /**
     * 一个单元格的宽度 单位：像素
     */
    public float gridCellWidth = 10; //一个单元格的宽度
    /**
     * 一个单元格的高度 单位：像素
     */
    public float gridCellHeight = 10;//一个单元格的高度
    /**
     * 标尺文本大小 单位：像素
     */
    public float textSize = 32;
    /**
     * 字体颜色
     */
    public int textColor = Color.BLACK;//字体颜色
    /**
     * 标记栏背景颜色
     */
    public int barColor = Color.argb(180, 255, 255, 0); //标记栏的背景颜色
    /**
     * 网格画笔宽度
     */
    public float gridStrokeWidth = 2;//画笔宽度
    /**
     * 面板背景颜色
     */
    public int panelBackgroundColor = Color.argb(255, 236, 236, 236);//面板背景颜色
    /**
     * 网格颜色
     */
    public int gridColor = Color.GRAY;//网格颜色

    /**
     * 打印头高度
     */
    public float printHeadHeight = 0;

    /**
     * 单位的背景颜色（最开头的小方块背景颜色）
     */
    public int unitBackgroundColor = Color.argb(255, 0, 170, 255);

    /**
     * 单位的字体颜色（最开头的小方块的字体颜色）
     */
    public int unitTextColor = Color.BLACK;

    /**
     * 单位的字体大小（最开头的小方块的字体大小）
     */
    public float unitTextSize = 32;

    /**
     * 单元的文本（目前只有两个单位）
     * mm:毫米
     * px:像素
     */
    public String unitText = "mm";

    /**
     * 是否固定坐标轴
     */
    public boolean isFixedAxes = true;

    /**
     * 刻度的大小
     */
    public float scaleSize = 2;
    /**
     * 刻度颜色
     */
    public int scaleColor = Color.GRAY;
}
