package com.yufea.scalepanel.config;

import android.graphics.Color;

/**
 * 计数配置
 *
 * @author peifen
 * @date 2020/5/18 12:56
 */
public class CountConfig extends ContentConfig {
    /**
     * 字体颜色
     */
    public int textColor = Color.BLACK;
    /**
     * 字体大小，建议使用sp
     * <p>
     * 请使用height和widht
     */
    @Deprecated
    public int textSize = 50;
    /**
     * 字体
     */
    public String fontFamily;
    /**
     * 字体类型 参考 Typeface
     */
    public int fontStyle = 0;

    /**
     * 位数
     */
    public int digit = 9;

    /**
     * 起始值
     */
    public long startValue = 0;

    /**
     * 终点值
     */
    public long endValue = 999999999;

    /**
     * 步长
     */
    public int step = 1;

    /**
     * 循环次数
     */
    public int loopCount = 1;

    /**
     * 单次循环次数
     */
    public int singleLoopCount = 1;

    /**
     * 是否显示0
     */
    public boolean isShowZero = true;

    /**
     * 当前计数
     */
    public Integer currentCount;
}
