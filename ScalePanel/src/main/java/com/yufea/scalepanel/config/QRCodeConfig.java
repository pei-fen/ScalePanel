package com.yufea.scalepanel.config;

import android.graphics.Color;

/**
 * 二维码属性
 *
 * @author peifen
 * @date 2020/5/18 12:56
 */
public class QRCodeConfig extends ContentConfig {
    /**
     * 文本
     */
    public String text;
    /**
     * 字体颜色
     */
    public int textColor = Color.BLACK;

    /**
     * 编码类型
     */
    public String encodeType = "QRCode";

    /**
     * 点阵级别
     */
    public int level = -1;

    /**
     * L(1),
     * M(0),
     * Q(3),
     * H(2);
     */
    public int errorCorrectionLevel = -1;

    /**
     * 可变数据
     */
    public String[] variableDatas;
}
