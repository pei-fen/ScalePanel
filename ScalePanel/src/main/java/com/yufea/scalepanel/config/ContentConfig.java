package com.yufea.scalepanel.config;

import android.graphics.Color;

import java.io.Serializable;

/**
 * @author peifen
 * @date 2020/5/18 12:56
 */
public class ContentConfig implements Serializable {
    /**
     * 缩放比例
     */
    public float scale = 1;

    /**
     * 内容的类型：1,文本  2，图片  3，条码   4，二维码   5，有效期  6，生产日期  7，计数
     */
    public int contentType = 1;
    /**
     * 背景颜色
     */
    public int background = Color.TRANSPARENT; //背景颜色
    /**
     * 边框颜色
     */
    public int borderColor = Color.BLACK;//边框颜色
    /**
     * 边框大小
     */
    public float borderSize = 2; //边框大小

    /**
     * 方向角度
     */
    public float directionDegress = 0;
    /**
     * X轴偏移量
     */
    public float directionOffsetX = 0;
    /**
     * Y轴偏移量
     */
    public float directionOffsetY = 0;

    /**
     * 宽度
     */
    public float width;

    /**
     * 是否等宽，如果为true，则width有效，如果为false，则width默认不生效
     */
    public boolean isFixedWidth = true;

    /**
     * 高度
     */
    public float height;

    /**
     * x轴坐标,不含包markerBarYWidth的宽度
     */
    public float x = 0;
    /**
     * y轴坐标，不包含markerBarXHeight的高度
     */
    public float y = 0;

    /**
     * 间隔
     */
    public float space = 0;

    /**
     * 上边距
     */
    public float marginTop = 0;
    /**
     * 左边距
     */
    public float marginLeft = 0;

    /**
     * 高度扩展
     */
    public float heightExtend = 0;

    /**
     * 内容可以超出上边界的距离
     */
    public float offsetTop = 0;
    /**
     * 内容可以超出左边界的距离
     */
    public float offsetLeft = 0;
    /**
     * 内容可以超出右边界的距离
     */
    public float offsetRight = 0;
    /**
     * 内容可以超出底部边界的距离
     */
    public float offsetBottom = 0;
    /**
     * 内容的颜色
     */
    public int contentColor = Color.BLACK;
}
