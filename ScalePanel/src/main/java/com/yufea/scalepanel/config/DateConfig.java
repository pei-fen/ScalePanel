package com.yufea.scalepanel.config;

import android.graphics.Color;

/**
 * 日期属性
 *
 * @author peifen
 * @date 2020/5/18 12:56
 */
public class DateConfig extends ContentConfig {

    /**
     * 日期文本
     */
    public String dateText = "";
    /**
     * 时间文本
     */
    public String timeText = "";

    /**
     * 是否显示日期
     */
    public boolean isDisplayDate = true;

    /**
     * 是否显示时间
     */
    public boolean isDisplayTime = true;

    /**
     * 日期格式
     */
    public String dateFormat = "yyyy-MM-dd";

    /**
     * 日期符号：如日期格式为yyyyaMMbddc  则数组中分别是  a，b，c
     */
    public String[] dateSymbols = new String[3];
    /**
     * 时间符号 如日期格式为hhammbssc  则数组中分别是  a，b，c
     */
    public String[] timeSymbols = new String[3];

    /**
     * 时间格式
     */
    public String timeFormat = "HH:mm:ss";

    /**
     * 字体颜色
     */
    public int textColor = Color.BLACK;
    /**
     * 字体大小，建议使用sp
     * <p>
     * 请使用height和widht
     */
    @Deprecated
    public int textSize = 50;
    /**
     * 字体
     */
    public String fontFamily;
    /**
     * 字体类型 参考 Typeface
     */
    public int fontStyle = 0;
}
