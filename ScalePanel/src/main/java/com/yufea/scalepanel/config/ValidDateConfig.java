package com.yufea.scalepanel.config;

/**
 * 有效期日期属性
 */
public class ValidDateConfig extends DateConfig {

    /**
     * 有效期年
     */
    public int validDateYear;
    /**
     * 有效期月
     */
    public int validDateMonth;
    /**
     * 有效期日
     */
    public int validDateDay;
}
