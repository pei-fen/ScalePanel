package com.yufea.scalepanel;

import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * @author peifen
 * @date 2020/6/12 16:02
 */
public class FontHelper {
    private static FontHelper mHelper;
    private Map<String, Typeface> mFontMap;

    private FontHelper() {
        mFontMap = new HashMap<>();
    }

    public static FontHelper getInstance() {
        if (mHelper == null) {
            mHelper = new FontHelper();
        }
        return mHelper;
    }

    public int getSize() {
        return mFontMap.size();
    }

    public void clearFont() {
        mFontMap.clear();
    }

    public void addFont(String fontName, Typeface typeface) {
        mFontMap.put(fontName, typeface);
    }

    public Typeface getFont(String fontName) {
        return mFontMap.get(fontName);
    }
}
