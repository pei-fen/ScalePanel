package com.yufea.scalepanel.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.datamatrix.encoder.SymbolShapeHint;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.yufea.scalepanel.BarcodeType;
import com.yufea.scalepanel.FontHelper;
import com.yufea.scalepanel.barcode.builder.EAN13Builder;
import com.yufea.scalepanel.barcode.builder.EAN8Builder;
import com.yufea.scalepanel.config.BarCodeConfig;
import com.yufea.scalepanel.config.ImageConfig;
import com.yufea.scalepanel.config.QRCodeConfig;
import com.yufea.scalepanel.config.TextConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author peifen
 * @date 2020/5/15 16:27
 */
public class BitmapUtil {

    private static final String TAG = BitmapUtil.class.getSimpleName();
    private static final Bitmap.Config mBitmapConfig = Bitmap.Config.ARGB_4444;

    /**
     * 文本转bitmap
     *
     * @param config 文本的配置参数
     * @return
     * @see BitmapUtil#convertToBitmapByWidthAndHeight(TextConfig)
     */
    @Deprecated
    public static Bitmap convertToBitmapBySize(TextConfig config) {
        Paint paint = new Paint();
        int offset = -2;
        Typeface typeface;
        if (config.fontFamily != null) {
            typeface = FontHelper.getInstance().getFont(config.fontFamily);
            if (typeface == null) {
                typeface = Typeface.DEFAULT;
            }
        } else {
            typeface = Typeface.create(Typeface.DEFAULT, config.fontStyle);
        }
        paint.setTypeface(Typeface.create(typeface, config.fontStyle));
        paint.setTextSize(config.textSize);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setColor(config.textColor);

        int width = (int) paint.measureText(config.text);
        Paint.FontMetricsInt fmi = paint.getFontMetricsInt();
//        int height = fmi.descent - fmi.ascent;
        int height = fmi.bottom - fmi.top;
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        int y = height / 2 + (fmi.bottom - fmi.top) / 2 - fmi.bottom - offset;
        canvas.drawText(config.text, 0, y, paint);
        canvas.save();
        return bitmap;
    }

    /**
     * 根据字体的宽度和高度转换为bitmap
     *
     * @return
     */
    public static Bitmap convertToBitmapByWidthAndHeight(TextConfig config) {
        float scaleWidth = config.width * config.scale;
        float scaleHeight = config.height * config.scale;
        float scaleMarginLeft = config.marginLeft * config.scale;
        float scaleMarginTop = config.marginTop * config.scale;
        float scaleSpace = config.space * config.scale;
        float scaleHeightExtend = config.heightExtend * config.scale;
        Paint paint = new Paint();
        Typeface typeface;
        if (config.fontFamily != null) {
            typeface = FontHelper.getInstance().getFont(config.fontFamily);
            if (typeface == null) {
                typeface = Typeface.DEFAULT;
            }
        } else {
            typeface = Typeface.DEFAULT;
        }
        paint.setTypeface(Typeface.create(typeface, config.fontStyle));
        paint.setTextSize(scaleHeight * 1.5f);//设置偏大一点，去边后缩小的时候就不会失真
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setColor(config.textColor);
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        char[] chars = config.text.toCharArray();
        float totalWidht = 0;
        float oHeight = 0;
        List<Bitmap> bitmaps = new ArrayList<>();
        for (int i = 0; i < chars.length; i++) {
            String s = chars[i] + "";
            float width = paint.measureText(s);
            float tempExtendWidth = 0;
            if ((config.fontStyle == Typeface.ITALIC || config.fontStyle == Typeface.BOLD_ITALIC) && !config.isFixedWidth) {//处理斜体或粗斜体显示不全
                tempExtendWidth = width * 0.45f;
//                width = width + tempExtendWidth;
            }
            Paint.FontMetrics fm = paint.getFontMetrics();
            float height = fm.bottom - fm.top;
            int y = (int) (height / 2 + (fm.bottom - fm.top) / 2 - fm.bottom);
            //开始绘制一个字符的bitmap
            if (config.isFixedWidth) {
                //根据比例获取width
                width = scaleWidth * height / scaleHeight;
            }
            Bitmap bitmap = Bitmap.createBitmap((int) (width + tempExtendWidth), (int) height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawText(s, (width - paint.measureText(s)) / 2, y, paint);

            canvas.save();
            bitmaps.add(bitmap);
            if (i == chars.length - 1) {
                totalWidht = totalWidht + bitmap.getWidth();
            } else {
                //根据比例获取space
                float space = scaleSpace * height / scaleHeight;
                totalWidht = totalWidht + bitmap.getWidth() + space;
                totalWidht -= tempExtendWidth;
            }
            if (i == 0) {
//                totalWidht = totalWidht - tempExtendWidth / 2f;
            }
            oHeight = height;
        }
        if (totalWidht <= 0) {
            totalWidht = 1;
        }
        Bitmap bitmap = Bitmap.createBitmap((int) totalWidht, (int) oHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        float calcWidht = 0;
        for (int i = 0; i < bitmaps.size(); i++) {
            String s = chars[i] + "";
            float space = scaleSpace * oHeight / scaleHeight;
            canvas.drawBitmap(bitmaps.get(i), calcWidht, 0, paint);
            calcWidht = calcWidht + bitmaps.get(i).getWidth() + space;
            if ((config.fontStyle == Typeface.ITALIC || config.fontStyle == Typeface.BOLD_ITALIC) && !config.isFixedWidth) {
                calcWidht = (int) (calcWidht - (bitmaps.get(i).getWidth() / 1.45f * 0.45f));
            }
        }
        canvas.save();
        bitmap = cleanPadding(bitmap);

        float newWidth = scaleHeight * bitmap.getWidth() / bitmap.getHeight();
        float newHeight = scaleHeight + scaleHeightExtend;
        if (scaleHeightExtend != 0) {
            int heightExtend = (int) (scaleHeightExtend * bitmap.getHeight() / scaleHeight);
            bitmap = zoomImg(bitmap, bitmap.getWidth(), bitmap.getHeight() + heightExtend);
        }
        //最后缩小，bitmap就不会因为放大了而失真
        bitmap = zoomImg(bitmap, newWidth, newHeight);

        if (config.isFixedWidth) {//处理因为去掉上下padding而导致的宽度变大
            bitmap = handleWidth(bitmap, config.text.length(), scaleWidth, scaleSpace);
        }
        //处理边距
        bitmap = handleMargin(bitmap, scaleMarginLeft, scaleMarginTop);

        if (config.contentType == 5 || config.contentType == 6 || config.contentType == 7) {
            bitmap = correctWidth(bitmap, config);
        }
        if (config.directionDegress != 0) {
            bitmap = rotateImg(bitmap, config.directionDegress);
        }
        return bitmap;
    }

    /**
     * 获取0-9字模数据
     *
     * @param config
     * @param scale
     * @return
     */
    public static List<Byte> get0to9WordModel(TextConfig config, float scale) {
        List<Byte> totalByteList = new ArrayList<>();
        if (TextUtils.isEmpty(config.text)) {
            config.text = "0123456789";
        }
        float tempDegress = config.directionDegress;
        config.directionDegress = 0;
        Bitmap bitmap = convertToBitmapByWidthAndHeight(config);
        bitmap = zoomImg(bitmap, bitmap.getWidth() / scale, config.height + config.heightExtend);
        int unitWidth = (int) (config.width + config.space);
        Log.e(TAG, "bitmap.width = " + bitmap.getWidth() + ", bitmap.height = " + bitmap.getHeight() + ",unitWidth = " + (config.width + config.space));
        for (int i = 0; i <= 9; i++) {
            int[] unitPiexls = new int[bitmap.getWidth() * bitmap.getHeight()];
            int unit = unitWidth;
//            if (i == 9) {
//                unit = (int) config.width;
//            }
            if (unitWidth * i + unit > bitmap.getWidth()) {
                unit = unit - (unitWidth * i + unit - bitmap.getWidth());
            }
            bitmap.getPixels(unitPiexls, 0, bitmap.getWidth(), (int) (unitWidth * i), 0, (int) unit, bitmap.getHeight());
            Bitmap unitBitmap = Bitmap.createBitmap(unitPiexls, 0, bitmap.getWidth(), (int) unit, bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            if (tempDegress != 0) {
                unitBitmap = rotateImg(unitBitmap, tempDegress);
            }
            byte[] fontModels = TypeMatrixUtil.getFontModel(unitBitmap, new FontModelConfig());
            for (byte b : fontModels) {
                totalByteList.add(b);
            }
        }
        return totalByteList;
    }

    /**
     * 处理宽度
     *
     * @param bitmap
     * @param fontCount
     * @param lastUnitWidth
     * @param space
     * @return
     */
    public static Bitmap handleWidth(Bitmap bitmap, int fontCount, float lastUnitWidth, float space) {
        float width = (int) (lastUnitWidth * fontCount + space * (fontCount - 1));
        float unnecessaryPart = bitmap.getWidth() - width; //多余部分
        float unitWidthScale = lastUnitWidth * fontCount / width;// 字体占总长度的比例
        float unitWidthUnnecessaryPart = unnecessaryPart * unitWidthScale / fontCount;//一个字体多余部分
        float spaceScale = space * (fontCount - 1) / width; //间距占总长度的比例
        float spaceUnnecessaryPart = unnecessaryPart * spaceScale / (fontCount - 1); //一个间隔多余部分
        int[] pixels = new int[(int) (width * bitmap.getHeight())];
        float x = 0;
        float unit = lastUnitWidth + space;
        for (int i = 0; i < fontCount; i++) {
            x += unitWidthUnnecessaryPart / 2;
            float w = unit;
            if (i == fontCount - 1) {
                w = lastUnitWidth;
            }
            bitmap.getPixels(pixels, (int) (unit * i), (int) width, (int) x, 0, (int) w, bitmap.getHeight());
            x += spaceUnnecessaryPart + unitWidthUnnecessaryPart / 2 + unit;
        }
        Bitmap newBitmap = Bitmap.createBitmap((int) width, bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        newBitmap.setPixels(pixels, 0, newBitmap.getWidth(), 0, 0, newBitmap.getWidth(), newBitmap.getHeight());
        return newBitmap;
    }

    /**
     * 处理边距
     *
     * @param bitmap
     * @param leftMargin
     * @param topMargin
     * @return
     */
    private static Bitmap handleMargin(Bitmap bitmap, float leftMargin, float topMargin) {
        if (leftMargin == 0 && topMargin == 0) {
            return bitmap;
        }
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawBitmap(bitmap, leftMargin, topMargin, new Paint());
        return newBitmap;
    }

    /**
     * 修正宽度
     * 因为最终结果是1mm = 1.84,不为整数，所以有效期0~9取模会出现问题。最终以单个字体+间距的宽度为整数
     *
     * @param bitmap
     * @param config
     * @return
     */
    public static Bitmap correctWidth(Bitmap bitmap, TextConfig config) {
        float w = config.width + config.space;
        //1毫米 == 11.8px
        int unitWidthPx = (int) (config.width + config.space);
        float unitWidth = unitWidthPx * config.scale * config.width / w;
        float space = unitWidthPx * config.scale * config.space / w;
        return BitmapUtil.handleWidth(bitmap, config.text.length(), unitWidth, space);
    }

    /**
     * 缩放图片
     *
     * @param bm        bitmap
     * @param newWidth  新的宽度
     * @param newHeight 新的高度
     * @return
     */
    public static Bitmap zoomImg(Bitmap bm, float newWidth, float newHeight) {
        newWidth = Math.round(newWidth);
        newHeight = Math.round(newHeight);
        // 获得图片的宽高
        int width = bm.getWidth();
        int height = bm.getHeight();
        // 计算缩放比例
        float scaleWidth = newWidth / width;
        float scaleHeight = newHeight / height;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
    }

    /**
     * 旋转图片
     *
     * @param bm      bitmap
     * @param degress 角度
     * @return
     */
    public static Bitmap rotateImg(Bitmap bm, float degress) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degress);
        Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, false);
        return newbm;
    }

    /**
     * 创建二维码
     *
     * @param config 二维码配置
     * @return bitmap
     */
    public static Bitmap createQrCode(QRCodeConfig config) {
        float scaleWidth = config.width * config.scale;
        float scaleHeight = config.height * config.scale;
        Bitmap bitmap = null;
        try {
            BarcodeFormat barcodeFormat = getBarcodeFormat(config.encodeType);
            if (barcodeFormat == BarcodeFormat.PDF_417) {
                Map<EncodeHintType, Object> hints = new HashMap<>();
                hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
                hints.put(EncodeHintType.MARGIN, 0);
                if (config.errorCorrectionLevel != -1) {
                    if (config.errorCorrectionLevel == ErrorCorrectionLevel.M.getBits()) {
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
                    } else if (config.errorCorrectionLevel == ErrorCorrectionLevel.L.getBits()) {
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
                    } else if (config.errorCorrectionLevel == ErrorCorrectionLevel.H.getBits()) {
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
                    } else if (config.errorCorrectionLevel == ErrorCorrectionLevel.Q.getBits()) {
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q);
                    }
                }
                BitMatrix matrix = new MultiFormatWriter().encode(config.text, getBarcodeFormat(config.encodeType), 2000, 2000, hints);
                bitmap = createBitmap(matrix);
                bitmap = zoomImg(bitmap, scaleWidth, scaleHeight);
            } else {
                Map<EncodeHintType, Object> hints = new HashMap<>();
                hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
                hints.put(EncodeHintType.MARGIN, 0);
                if (config.encodeType.equalsIgnoreCase(BarcodeType.DATA_MATRIX)) {
                    hints.put(EncodeHintType.DATA_MATRIX_SHAPE, SymbolShapeHint.FORCE_SQUARE);
                }
                if (config.errorCorrectionLevel != -1) {
                    if (config.errorCorrectionLevel == ErrorCorrectionLevel.M.getBits()) {
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
                    } else if (config.errorCorrectionLevel == ErrorCorrectionLevel.L.getBits()) {
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
                    } else if (config.errorCorrectionLevel == ErrorCorrectionLevel.H.getBits()) {
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
                    } else if (config.errorCorrectionLevel == ErrorCorrectionLevel.Q.getBits()) {
                        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q);
                    }
                }
                BitMatrix matrix = new MultiFormatWriter().encode(config.text, getBarcodeFormat(config.encodeType), (int) scaleWidth, (int) scaleHeight, hints);
                bitmap = createBitmap(matrix);
            }
            if (config.directionDegress != 0) {
                bitmap = rotateImg(bitmap, config.directionDegress);
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * 创建一个条形码
     *
     * @param config 条形码配置
     * @return bitmap
     */
    public static Bitmap createBarCode(BarCodeConfig config) {
        float scaleWidth = config.width * config.scale;
        float scaleHeight = config.height * config.scale;
        Bitmap bitmap = null;
        try {
            if (config.encodeType.equalsIgnoreCase(BarcodeType.EAN_13)) {
                EAN13Builder ean13Builder = new EAN13Builder();
                bitmap = ean13Builder.generateBarcode(config);
                return zoomImg(bitmap, scaleWidth, scaleHeight);
            } else if (config.encodeType.equalsIgnoreCase(BarcodeType.EAN_8)) {
                EAN8Builder ean8Builder = new EAN8Builder();
                bitmap = ean8Builder.generateBarcode(config);
                return zoomImg(bitmap, scaleWidth, scaleHeight);
            }
            //先判断是否显示文本
            //是否显示文本
            Bitmap textBitmap = null;
            if (config.isDisplayText) {
                TextConfig textConfig = new TextConfig();
                textConfig.scale = config.scale;
                textConfig.text = config.text;
                textConfig.height = config.height * 0.22f;
                textConfig.isFixedWidth = false;
//                textConfig.width = barCodeConfig.textWidth;
                textBitmap = convertToBitmapByWidthAndHeight(textConfig);
            }
            float barCodeHeight = scaleHeight;
            if (textBitmap != null) {
                barCodeHeight = scaleHeight - scaleHeight * 0.23f;
            }
            Map<EncodeHintType, Object> hints = new HashMap<>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            hints.put(EncodeHintType.MARGIN, 0);
            BitMatrix matrix = new MultiFormatWriter().encode(config.text, getBarcodeFormat(config.encodeType), (int) scaleWidth, (int) barCodeHeight, hints);
            Bitmap barCodeBitmap = createBitmap(matrix);

            bitmap = Bitmap.createBitmap((int) scaleWidth, (int) scaleHeight, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            canvas.drawBitmap(barCodeBitmap, 0, 0, paint);
            if (textBitmap != null) {
                float left = 0;
                if (config.textAlign == 1) {//左对齐
                    left = 0;
                } else if (config.textAlign == 2) {//右对齐
                    left = bitmap.getWidth() - textBitmap.getWidth();
                } else if (config.textAlign == 3) {//居中
                    left = (bitmap.getWidth() - textBitmap.getWidth()) / 2f;
                }
                canvas.drawBitmap(textBitmap, left, scaleHeight * 0.78f, paint);
            }
            canvas.save();
            if (config.directionDegress != 0) {
                bitmap = rotateImg(bitmap, config.directionDegress);
            }
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * 获取条码格式
     *
     * @param encodeType
     * @return
     */
    private static BarcodeFormat getBarcodeFormat(String encodeType) {
        BarcodeFormat format = null;
        if (encodeType.equalsIgnoreCase(BarcodeType.CODE_39)) {
            format = BarcodeFormat.CODE_39;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.CODE_128)) {
            format = BarcodeFormat.CODE_128;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.ITF)) {
            format = BarcodeFormat.ITF;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.CODABAR)) {
            format = BarcodeFormat.CODABAR;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.EAN_8)) {
            format = BarcodeFormat.EAN_8;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.EAN_13)) {
            format = BarcodeFormat.EAN_13;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.QR_CODE)) {
            format = BarcodeFormat.QR_CODE;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.PDF_417)) {
            format = BarcodeFormat.PDF_417;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.DATA_MATRIX)) {
            format = BarcodeFormat.DATA_MATRIX;
        } else if (encodeType.equalsIgnoreCase(BarcodeType.AZTEC)) {
            format = BarcodeFormat.AZTEC;
        }
        return format;
    }

    private static final int WHITE = 0x00FFFFFF; //透明
    private static final int BLACK = 0xFF000000;

    /**
     * 创建bitmap
     *
     * @param matrix
     * @return
     */
    private static Bitmap createBitmap(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    /**
     * 转换成黑色图片
     *
     * @param bitmap
     * @return
     */
    public static Bitmap convertBlackBitmap(Bitmap bitmap) {
        int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        for (int y = 0; y < bitmap.getHeight(); y++) {
            for (int x = 0; x < bitmap.getWidth(); x++) {
                int pixel = pixels[y * bitmap.getWidth() + x];//取出当前位置的像素值（颜色）
                int a = (pixel >> 24) & 0xFF;
                int r = (pixel >> 16) & 0xFF;
                int g = (pixel >> 8) & 0xFF;
                int b = pixel & 0xFF;
                int color = Color.TRANSPARENT;
                if ((r + g + b) < 600 && a > 150) {
                    color = Color.BLACK;
                }
                newBitmap.setPixel(x, y, color);
            }
        }
        return newBitmap;
    }

    /**
     * 创建图片
     *
     * @param config 图片配置
     * @return
     */
    public static Bitmap createBitmap(ImageConfig config) {
        float scaleWidth = config.width * config.scale;
        float scaleHeight = config.height * config.scale;
        Bitmap bitmap = config.originalBitmap;
        if (bitmap == null) {
            FontModelConfig fontModelConfig = new FontModelConfig();
            fontModelConfig.h = (int) config.originalImageHeight;
            fontModelConfig.w = (int) config.originalImageWidth;
            bitmap = TypeMatrixUtil.convertToBitmap(config.fontModels, fontModelConfig);
        }
        bitmap = zoomImg(bitmap, scaleWidth, scaleHeight);
        if (config.directionDegress != 0) {
            bitmap = rotateImg(bitmap, config.directionDegress);
        }
        return bitmap;
    }

    private static Bitmap cleanPadding(Bitmap bitmap) {
        int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        int topPadding = 0, leftPadding = 0, rightPadding = 0, bottomPadding = 0;
        boolean isBreak = false;
        //获取topPadding
        for (int h = 0; h < bitmap.getHeight(); h++) {
            for (int w = 0; w < bitmap.getWidth(); w++) {
                int pixel = pixels[h * bitmap.getWidth() + w];
                int a = (pixel >> 24) & 0xFF;
                if (a != 0) { //透明
                    isBreak = true;
                    break;
                }
            }
            if (isBreak) {
                break;
            }
            topPadding++;
        }
        if (topPadding > 5) {
            topPadding = topPadding - 5;
        }
        isBreak = false;
        //获取bottomPadding
        for (int h = bitmap.getHeight() - 1; h >= 0; h--) {
            for (int w = 0; w < bitmap.getWidth(); w++) {
                int pixel = pixels[h * bitmap.getWidth() + w];
                int a = (pixel >> 24) & 0xFF;
                if (a != 0) { //透明
                    isBreak = true;
                    break;
                }
            }
            if (isBreak) {
                break;
            }
            bottomPadding++;
        }
        if (bottomPadding > 5) {
            bottomPadding = bottomPadding - 5;
        }
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth() - rightPadding - leftPadding, bitmap.getHeight() - topPadding - bottomPadding, Bitmap.Config.ARGB_8888);
        int[] newPixels = new int[newBitmap.getHeight() * newBitmap.getWidth()];
        bitmap.getPixels(newPixels, 0, newBitmap.getWidth(), rightPadding, topPadding, newBitmap.getWidth(), newBitmap.getHeight());
        newBitmap.setPixels(newPixels, 0, newBitmap.getWidth(), 0, 0, newBitmap.getWidth(), newBitmap.getHeight());
        return newBitmap;
    }

    /**
     * 保存bitmap到本地
     *
     * @param bm       bitmap
     * @param descFile 保存的文件
     */
    public static void saveBitmap(Bitmap bm, File descFile) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(descFile);
            bm.compress(Bitmap.CompressFormat.PNG, 80, fos);
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
