package com.yufea.scalepanel.utils;

import java.io.UnsupportedEncodingException;

/**
 * byte工具类
 *
 * @author peifen
 * @date 2020/5/13 10:18
 */
public class ByteUtil {

    /**
     * bytes数组转为十六进制字符串
     *
     * @param bytes byte集合
     * @return 十六进制字符串
     */
    public static String bytesToHexString(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (bytes == null) {
            return null;
        }
        if (bytes.length <= 0) {
            return "";
        }
        for (int i = 0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * 十六进制字符串转byte数组
     *
     * @param hexString 十六进制字符串
     * @return byte数组
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null) {
            return null;
        }
        if (hexString.equals("")) {
            return new byte[0];
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * int到byte[] 由高位到低位
     *
     * @param i 需要转换为byte数组的整行值。
     * @return byte数组
     */
    public static byte[] intToByteArray(int i) {
        byte[] result = new byte[4];
        result[0] = (byte) ((i >> 24) & 0xFF);
        result[1] = (byte) ((i >> 16) & 0xFF);
        result[2] = (byte) ((i >> 8) & 0xFF);
        result[3] = (byte) (i & 0xFF);
        return result;
    }

    public static void main(String args[]) throws UnsupportedEncodingException {
        byte[] bytes = "ab12阿".getBytes("GB2312");
        for (byte b : bytes) {
            System.out.println(Integer.toHexString(b));
        }
        System.out.println(new String(bytes,"GB2312"));

//        byte[] bytes2 = "35a".getBytes("ASCII");
//        for (byte b : bytes2) {
//            System.out.println(Integer.toHexString(b));
//        }
////
        byte[] str = new byte[4];
        str[0] = (byte) Integer.parseInt("28", 16);
        str[1] = (byte) Integer.parseInt("33", 16);
        str[2] = (byte) Integer.parseInt("b0", 16);
        str[3] = (byte) Integer.parseInt("a2", 16);
//        str[3] = (byte) Integer.parseInt("a2", 16);
//        str[4] = (byte) Integer.parseInt("a3", 16);
//        str[5] = (byte) Integer.parseInt("b4", 16);
//        str[6] = (byte) Integer.parseInt("a3", 16);
//        str[7] = (byte) Integer.parseInt("c1", 16);
//        str[6] = (byte) Integer.parseInt("a3", 16);
//        str[7] = (byte) Integer.parseInt("e1", 16);
        System.out.println(new String(str, "GB2312"));
//        System.out.println("343 的3３３３ａa");
    }

    /**
     * byte[]转int
     *
     * @param bytes 需要转换成int的数组
     * @return int值
     */
    public static int byteArrayToInt(byte[] bytes) {
        int value = 0;
        for (int i = 0; i < 4; i++) {
            int shift = (3 - i) * 8;
            value += (bytes[i] & 0xFF) << shift;
        }
        return value;
    }

    /**
     * 十六进制字符转byte
     *
     * @param c 十六进制字符 0到F
     * @return
     */
    public static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }


    /**
     * 二进制字符串转字节数组，如 101000000100100101110000 -> A0 09 70
     *
     * @param input 输入字符串。
     * @return 转换好的字节数组。
     */
    static byte[] binaryStrToBytes(String input) {
        StringBuilder in = new StringBuilder(input);
        // 注：这里in.length() 不可在for循环内调用，因为长度在变化
        int remainder = in.length() % 8;
        if (remainder > 0)
            for (int i = 0; i < 8 - remainder; i++)
                in.append("0");
        byte[] bts = new byte[in.length() / 8];

        // Step 8 Apply compression
        for (int i = 0; i < bts.length; i++)
            bts[i] = (byte) Integer.parseInt(in.substring(i * 8, i * 8 + 8), 2);

        return bts;
    }

    /**
     * 字节数组转二进制字符串，如 A0 09 70 -> 101000000000100101110000。
     *
     * @param bts 转入字节数组。
     * @return 转换好的只有“1”和“0”的字符串。
     */
    public static String bytesToBinaryStr(byte[] bts) {
        String[] dic = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
                "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"};
        StringBuilder out = new StringBuilder();
        for (byte b : bts) {
            String s = String.format("%x", b);
            s = s.length() == 1 ? "0" + s : s;
            out.append(dic[Integer.parseInt(s.substring(0, 1), 16)]);
            out.append(dic[Integer.parseInt(s.substring(1, 2), 16)]);
        }
        return out.toString();
    }
}