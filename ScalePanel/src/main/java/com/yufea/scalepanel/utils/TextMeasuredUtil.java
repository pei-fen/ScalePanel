package com.yufea.scalepanel.utils;

import android.graphics.Paint;
import android.graphics.Typeface;

import com.yufea.scalepanel.FontHelper;
import com.yufea.scalepanel.config.TextConfig;

/**
 * 文本测量工具
 *
 * @author peifen
 * @date 2020/5/15 17:03
 */
public class TextMeasuredUtil {
    /**
     * 获取文本的宽度
     *
     * @param config 文本参数
     * @return
     */
    public static int getTextWidth(TextConfig config) {
        Paint paint = new Paint();
        paint.setTextSize(config.textSize);
        Typeface typeface = Typeface.DEFAULT;
        if (config.fontFamily != null) {
            typeface = FontHelper.getInstance().getFont(config.fontFamily);
            if (typeface != null) {
                typeface = Typeface.create(typeface, config.fontStyle);
            } else {
                typeface = Typeface.create(Typeface.DEFAULT, config.fontStyle);
            }
        }
        paint.setTypeface(typeface);
        return (int) paint.measureText(config.text);
    }

    /**
     * 获取文本的高度
     *
     * @param config 文本参数
     * @return
     */
    public static int getTextHeight(TextConfig config) {
        Paint paint = new Paint();
        paint.setTextSize(config.textSize);
        Typeface typeface = Typeface.DEFAULT;
        if (config.fontFamily != null) {
            typeface = FontHelper.getInstance().getFont(config.fontFamily);
            if (typeface != null) {
                typeface = Typeface.create(typeface, config.fontStyle);
            } else {
                typeface = Typeface.create(Typeface.DEFAULT, config.fontStyle);
            }
        }
        paint.setTypeface(typeface);
        Paint.FontMetricsInt fmi = paint.getFontMetricsInt();
//        return fmi.descent - fmi.ascent;
        return fmi.bottom - fmi.top;
    }
}
