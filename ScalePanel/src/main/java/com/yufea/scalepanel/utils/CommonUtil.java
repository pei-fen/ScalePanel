package com.yufea.scalepanel.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author peifen
 * @date 2020/8/12 16:17
 */
public class CommonUtil {
    public static <T> T cloneObject(T obj) {
        T result = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        ByteArrayInputStream byteArrayInputStream = null;
        ObjectOutputStream outputStream = null;
        ObjectInputStream inputStream = null;
        try {
            //对象写到内存中
            byteArrayOutputStream = new ByteArrayOutputStream();
            outputStream = new ObjectOutputStream(byteArrayOutputStream);
            outputStream.writeObject(obj);

            //从内存中再读出来
            byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            inputStream = new ObjectInputStream(byteArrayInputStream);
            result = (T) inputStream.readObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (outputStream != null)
                    outputStream.close();
                if (inputStream != null)
                    inputStream.close();
                if (byteArrayOutputStream != null)
                    byteArrayOutputStream.close();
                if (byteArrayInputStream != null)
                    byteArrayInputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 获取常用格式的标志位
     *
     * @param format 格式
     * @return 如果为null，则是自定义格式
     */
    public static Integer getDateFlag(String format) {
        Integer flag = null;
        switch (format) {
            case "yyyy/MM/dd":
                flag = 0;
                break;
            case "yyyy-MM-dd":
                flag = 1;
                break;
            case "yyyy.MM.dd":
                flag = 2;
                break;
            case "yyyyMMdd":
                flag = 3;
                break;
            case "yy/MM/dd":
                flag = 4;
                break;
            case "yy-MM-dd":
                flag = 5;
                break;
            case "yy.MM.dd":
                flag = 6;
                break;
            case "yyMMdd":
                flag = 7;
                break;
        }
        return flag;
    }
}
