package com.yufea.scalepanel.utils;

/**
 * 字模型配置
 *
 * @author peifen
 * @date 2020/6/9 16:18
 */
public class FontModelConfig {
    /**
     * 取模方式 1：逐列式   2:逐行式  3：列行式 4:行列式
     */
    public int way = 2;

    /**
     * 取模走向：为true：逆向   false:顺向
     */
    public boolean isReverse = false;
    /**
     * 宽度：字模转bitmap需要用到
     */
    public int w;
    /**
     * 高度：字模转bitmap需要用到
     */
    public int h;
}
