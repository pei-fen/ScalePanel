package com.yufea.scalepanel.utils;

/**
 * @author peifen
 * @date 2020/8/4 17:47
 */
public class BarcodeUtil {
    /**
     * 获取标注的UPC、EAN校验码
     *
     * @param s
     * @return
     */
    public static int getStandardUPCEANChecksum(CharSequence s) {
        int length = s.length();
        int sum = 0;
        for (int i = length - 1; i >= 0; i -= 2) {
            int digit = s.charAt(i) - '0';
            if (digit < 0 || digit > 9) {
                throw new IllegalArgumentException("格式错误：" + s.toString());
            }
            sum += digit;
        }
        sum *= 3;
        for (int i = length - 2; i >= 0; i -= 2) {
            int digit = s.charAt(i) - '0';
            if (digit < 0 || digit > 9) {
                throw new IllegalArgumentException("格式错误：" + s.toString());
            }
            sum += digit;
        }
        return (1000 - sum) % 10;
    }
}
