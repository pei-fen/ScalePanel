package com.yufea.scalepanel.utils;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * 字模工具类
 *
 * @author peifen
 * @date 2020/5/29 15:47
 */
public class TypeMatrixUtil {

    private static final String TAG = TypeMatrixUtil.class.getSimpleName();

    /**
     * 将字模集合转为bitmap
     *
     * @param fontModels 字模集合
     * @param config     取模方式
     * @return bitmap
     */
    public static Bitmap convertToBitmap(byte[] fontModels, FontModelConfig config) {
        Bitmap bitmap = Bitmap.createBitmap(config.w, config.h, Bitmap.Config.ARGB_8888);
        String binaryStr = ByteUtil.bytesToBinaryStr(fontModels);
        char[] binarys = binaryStr.toCharArray();
        long tempCount = 0;
        long startTime = System.currentTimeMillis();
        if (config.way == 1) {//逐列式
            //逐列式
            int height = binarys.length / config.w;
            StringBuilder binaryStrBuilder = new StringBuilder();
            for (int x = 0; x < config.w; x++) {
                for (int y = 0; y < height; y++) {
                    char binary = binarys[x * height + y];
                    binaryStrBuilder.append(Integer.parseInt(binary + ""));
                    if (binaryStrBuilder.length() % 8 == 0) {
                        if (config.isReverse) {//逆向
                            binaryStrBuilder.reverse();
                        }
                        for (int i = 0; i < binaryStrBuilder.length(); i++) {
                            Log.e("binary ", binary + "");
                            int color = Integer.parseInt(binaryStrBuilder.substring(i, i + 1)) == 1 ? Color.BLACK : Color.TRANSPARENT;
                            if (y < config.h) {
                                bitmap.setPixel(x, y - 9 + i, color);
                            }
                        }
                        binaryStrBuilder.delete(0, binaryStrBuilder.length());
                    }
                }
            }
        } else if (config.way == 2) { //逐行式
            int width = binarys.length / config.h;
            StringBuilder binaryStrBuilder = new StringBuilder();
            for (int y = 0; y < config.h; y++) {
                for (int x = 0; x < width; x++) {
                    char binary = binarys[y * width + x];
                    binaryStrBuilder.append(Integer.parseInt(binary + ""));
                    if (binaryStrBuilder.length() % 8 == 0) {
                        if (config.isReverse) {//逆向
                            binaryStrBuilder.reverse();
                        }
                        for (int i = 0; i < binaryStrBuilder.length(); i++) {
                            int color = Integer.parseInt(binaryStrBuilder.substring(i, i + 1)) == 1 ? Color.BLACK : Color.TRANSPARENT;
                            int xIndex = x + i - 7;
                            if (xIndex < config.w) {
                                bitmap.setPixel(xIndex, y, color);
                                tempCount++;
                            }
                        }
                        binaryStrBuilder.delete(0, binaryStrBuilder.length());
                    }
                }
            }
        } else if (config.way == 3) { //列行式
            int height = binarys.length / config.w;
            int count = height / 8;
            StringBuilder binaryStrBuilder = new StringBuilder();
            for (int i = 0; i < count; i++) {
                for (int x = 0; x < config.w; x++) {
                    for (int y = i * 8; y < height; y++) {
                        if (y >= i * 8 + 8) {//一列一次只取8个
                            break;
                        }
                        char binary = binarys[i * config.w * 8 + x * 8 + (y - i * 8)];
                        binaryStrBuilder.append(Integer.parseInt(binary + ""));
                        if (binaryStrBuilder.length() % 8 == 0) {
                            if (!config.isReverse) {//逆向
                                binaryStrBuilder.reverse();
                            }
                            for (int j = 0; j < binaryStrBuilder.length(); j++) {
                                int color = Integer.parseInt(binaryStrBuilder.substring(j, j + 1)) == 1 ? Color.BLACK : Color.TRANSPARENT;
                                if (y < config.h) {
                                    bitmap.setPixel(x, y - 7 + j, color);
                                }
                            }
                            binaryStrBuilder.delete(0, binaryStrBuilder.length());
                        }
                    }
                }
            }
        } else if (config.way == 4) { //列行式
            int width = binarys.length / config.h;
            int count = width / 8;
            StringBuilder binaryStrBuilder = new StringBuilder();
            for (int i = 0; i < count; i++) {
                for (int y = 0; y < config.h; y++) {
                    for (int x = i * 8; x < width; x++) {
                        if (x >= i * 8 + 8) {//一列一次只取8个
                            break;
                        }
                        char binary = binarys[i * config.h * 8 + y * 8 + (x - i * 8)];
                        binaryStrBuilder.append(Integer.parseInt(binary + ""));
                        if (binaryStrBuilder.length() % 8 == 0) {
                            if (!config.isReverse) {//逆向
                                binaryStrBuilder.reverse();
                            }
                            for (int j = 0; j < binaryStrBuilder.length(); j++) {
                                int color = Integer.parseInt(binaryStrBuilder.substring(j, j + 1)) == 1 ? Color.BLACK : Color.TRANSPARENT;
                                if (x < config.w) {
                                    bitmap.setPixel(x - 7 + j, y, color);
                                }
                            }
                            binaryStrBuilder.delete(0, binaryStrBuilder.length());
                        }
                    }
                }
            }
        }
        return bitmap;
    }

    /**
     * 转换bitmap图片为byte集合（字模集合）
     *
     * @param bitmap bitmap
     * @param config 取模方式：1：逐列式   3：列行式
     * @return 字模集合
     */
    public static byte[] getFontModel(Bitmap bitmap, FontModelConfig config) {
        if (bitmap == null) {
            return new byte[0];
        }
        int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        byte[][] bitmapModels = new byte[bitmap.getHeight()][bitmap.getWidth()];
        bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        for (int h = 0; h < bitmap.getHeight(); h++) {
            for (int w = 0; w < bitmap.getWidth(); w++) {
                int pixel = pixels[h * bitmap.getWidth() + w];//取出当前位置的像素值（颜色）
                int a = (pixel >> 24) & 0xFF;
                int r = (pixel >> 16) & 0xFF;
                int g = (pixel >> 8) & 0xFF;
                int b = pixel & 0xFF;
                if (a <= 50) {
                    continue;
                }
                if ((r + g + b) < 600) {
                    bitmapModels[h][w] = 1;
                }
            }
        }
        byte[] fontModels = null;
        if (config.way == 1) {//逐列式
            //逐列式模式，从第一列开始向下取8个点作为一个字节，如果最后不足8个点就补满8位
            int remainderBy8 = bitmap.getHeight() % 8;
            int newH = bitmap.getHeight();
            if (remainderBy8 > 0) {
                newH = newH - remainderBy8 + 8;
            }
            List<Byte> fontModelList = new ArrayList<>();
            byte[][] newBitmapModels = new byte[newH][bitmap.getWidth()];
            copyArray(bitmapModels, newBitmapModels);
            StringBuilder binaryStrBuilder = new StringBuilder();
            for (int w = 0; w < bitmap.getWidth(); w++) {
                for (int h = 0; h < newH; h++) {
                    binaryStrBuilder.append(newBitmapModels[h][w]);
                    if (binaryStrBuilder.length() % 8 == 0) {
                        String s = binaryStrBuilder.toString();
                        if (config.isReverse) {
                            s = binaryStrBuilder.reverse().toString();
                        }
                        byte[] tempBytes = ByteUtil.binaryStrToBytes(s);
                        for (byte b : tempBytes) {
                            fontModelList.add(b);
                        }
                        binaryStrBuilder.delete(0, binaryStrBuilder.length());
                    }
                }
            }
//            fontModels = ByteUtil.binaryStrToBytes(binaryStrBuilder.toString());
            fontModels = new byte[fontModelList.size()];
            for (int i = 0; i < fontModels.length; i++) {
                fontModels[i] = fontModelList.get(i);
            }
        } else if (config.way == 2) {//逐行式
            //从第一行开始向右每取8个点作为一个字节，如果最后不足8个点就补满8位。取模顺序是从高到低，即第一个点作为最高位。
            // 如*-------取为10000000
            int remainderBy8 = bitmap.getWidth() % 8;
            int newW = bitmap.getWidth();
            if (remainderBy8 > 0) {
                newW = newW - remainderBy8 + 8;
            }
            List<Byte> fontModelList = new ArrayList<>();
            byte[][] newBitmapModels = new byte[bitmap.getHeight()][newW];
            copyArray(bitmapModels, newBitmapModels);
            StringBuilder binaryStrBuilder = new StringBuilder();
            for (int h = 0; h < bitmap.getHeight(); h++) {
                for (int w = 0; w < newW; w++) {
                    binaryStrBuilder.append(newBitmapModels[h][w]);
                    if (binaryStrBuilder.length() % 8 == 0) {
                        String s = binaryStrBuilder.toString();
                        if (config.isReverse) {
                            s = binaryStrBuilder.reverse().toString();
                        }
                        byte[] tempBytes = ByteUtil.binaryStrToBytes(s);
                        for (byte b : tempBytes) {
                            fontModelList.add(b);
                        }
                        binaryStrBuilder.delete(0, binaryStrBuilder.length());
                    }
                }
            }
//            fontModels = ByteUtil.binaryStrToBytes(binaryStrBuilder.toString());
            fontModels = new byte[fontModelList.size()];
            for (int i = 0; i < fontModels.length; i++) {
                fontModels[i] = fontModelList.get(i);
            }
        } else if (config.way == 3) {//列行式
            //从第一列开始向下取8个点作为一个字节，然后从第二列开始向下取8个点作为第二个字节...依此类推。如果最后不足8个点就补满8位。取模顺序是从高到低，即第一个点作为最高位。如*-------取
            //为10000000
            int remainderBy8 = bitmap.getHeight() % 8;
            int newH = bitmap.getHeight();
            if (remainderBy8 > 0) {
                newH = newH - remainderBy8 + 8;
            }
            List<Byte> fontModelList = new ArrayList<>();
            byte[][] newBitmapModels = new byte[newH][bitmap.getWidth()];
            copyArray(bitmapModels, newBitmapModels);
            StringBuilder binaryStrBuilder = new StringBuilder();
            int count = newH / 8;
            for (int i = 0; i < count; i++) {
                for (int w = 0; w < bitmap.getWidth(); w++) {
                    for (int h = i * 8; h < newH; h++) {
                        if (h >= i * 8 + 8) {//一列一次只取8个
                            break;
                        }
                        binaryStrBuilder.append(newBitmapModels[h][w]);
                        if (binaryStrBuilder.length() % 8 == 0) {
                            String s = binaryStrBuilder.toString();
                            if (config.isReverse) {
                                s = binaryStrBuilder.reverse().toString();
                            }
                            byte[] tempBytes = ByteUtil.binaryStrToBytes(s);
                            for (byte b : tempBytes) {
                                fontModelList.add(b);
                            }
                            binaryStrBuilder.delete(0, binaryStrBuilder.length());
                        }
                    }
                }
            }
            fontModels = new byte[fontModelList.size()];
            for (int i = 0; i < fontModels.length; i++) {
                fontModels[i] = fontModelList.get(i);
            }
        } else if (config.way == 4) {//行列式
            //从第一行开始向右取8个点作为一个字节，然后从第二行开始向右取8个点作为第二个字节...依此类推。如果最后不足8个点就补满8位。   取模顺序是从低到高，即第一个点作为最
            //低位。如*-------取为00000001
            int remainderBy8 = bitmap.getWidth() % 8;
            int newW = bitmap.getWidth();
            if (remainderBy8 > 0) {
                newW = newW - remainderBy8 + 8;
            }
            List<Byte> fontModelList = new ArrayList<>();
            byte[][] newBitmapModels = new byte[bitmap.getHeight()][newW];
            copyArray(bitmapModels, newBitmapModels);
            StringBuilder binaryStrBuilder = new StringBuilder();
            int count = newW / 8;
            for (int i = 0; i < count; i++) {
                for (int h = 0; h < bitmap.getHeight(); h++) {
                    for (int w = i * 8; w < newW; w++) {
                        if (w >= i * 8 + 8) {//一列一次只取8个
                            break;
                        }
                        binaryStrBuilder.append(newBitmapModels[h][w]);
                        if (binaryStrBuilder.length() % 8 == 0) {
                            String s = binaryStrBuilder.toString();
                            if (config.isReverse) {
                                s = binaryStrBuilder.reverse().toString();
                            }
                            byte[] tempBytes = ByteUtil.binaryStrToBytes(s);
                            for (byte b : tempBytes) {
                                fontModelList.add(b);
                            }
                            binaryStrBuilder.delete(0, binaryStrBuilder.length());
                        }
                    }
                }
            }
            fontModels = new byte[fontModelList.size()];
            for (int i = 0; i < fontModels.length; i++) {
                fontModels[i] = fontModelList.get(i);
            }
        }
        return fontModels;
    }

    /**
     * 复制byte数组
     *
     * @param oBytes 原数组
     * @param tBytes 目标数组
     */
    private static void copyArray(byte[][] oBytes, byte[][] tBytes) {
        for (int i = 0; i < oBytes.length; i++) {
            for (int j = 0; j < oBytes[i].length; j++) {
                if (tBytes.length > i && tBytes[i].length > j) {
                    tBytes[i][j] = oBytes[i][j];
                }
            }
        }
    }
}
