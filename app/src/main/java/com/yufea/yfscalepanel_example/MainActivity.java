package com.yufea.yfscalepanel_example;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.yufea.scalepanel.PanelManager;
import com.yufea.scalepanel.config.PanelConfig;
import com.yufea.scalepanel.config.TextConfig;
import com.yufea.yfscalepanel_example.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mBinding;
    private PanelManager mPanelManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.setMainActivity(this);
        initPanelManager();
    }

    private void initPanelManager() {
        /*
         *创建一个新的面板管理，此类包含了以下功能
         *1、绘制刻度面板 drawPanel(final PanelConfig config, boolean isFixedAxes)
         *2、面板点击监听 setPanelOnClickListener
         *
         */
        mPanelManager = PanelManager.newInstance(this, mBinding.rlScaleplateContainer);

        mPanelManager.drawPanel(getCommonConfig(this), true);
        mPanelManager.setPanelOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "点击了面板...", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * 获取通用的PanelConfig
     *
     * @param context 上下文
     * @return PanelConfig
     */
    private PanelConfig getCommonConfig(Context context) {

        Resources res = context.getResources();
        PanelConfig config = new PanelConfig();
        config.markerBarYWidth = res.getDimension(R.dimen.dp30);
        config.markerBarXHeight = res.getDimension(R.dimen.dp30);
        config.barColor = Color.parseColor("#DDEBFF");

        config.widht = res.getDimension(R.dimen.dp1000) + config.markerBarYWidth;
        config.height = res.getDimension(R.dimen.dp800) + config.markerBarXHeight;
        config.gridCellWidth = res.getDimension(R.dimen.dp10);
        config.gridCellHeight = res.getDimension(R.dimen.dp10);
        config.gridStrokeWidth = (int) res.getDimension(R.dimen.dp1);
        config.textColor = Color.parseColor("#666666");
        config.gridColor = Color.GRAY;
        config.textSize = res.getDimension(R.dimen.font_size_3);
        config.panelBackgroundColor = Color.parseColor("#EEF5FE");
        config.printHeadHeight = res.getDimension(R.dimen.dp50);
        config.scaleSize = res.getDimension(R.dimen.dp2);
        config.scaleColor = Color.GRAY;

        config.unitText = "mm";
        config.unitTextSize = res.getDimension(R.dimen.font_size_3);
        config.unitBackgroundColor = Color.parseColor("#DDEBFF");
        return config;

    }

    public void onClickAddContent() {
        TextConfig textConfig = new TextConfig();
        textConfig.contentType = 1;
        textConfig.height = getResources().getDimension(R.dimen.dp50);
        textConfig.width = getResources().getDimension(R.dimen.dp50);
        textConfig.text = "添加内容";
        textConfig.isFixedWidth = false;
        textConfig.scale = 1;
        mPanelManager.addContentView(textConfig);
        Toast.makeText(this, "点击添加内容2", Toast.LENGTH_LONG).show();
        Toast.makeText(this, "hahahhehehe", Toast.LENGTH_LONG).show();

    }

    public void onClickRemoveSelectedContent() {
        mPanelManager.deleteContentView(mPanelManager.getSelectedContentView());
    }
}